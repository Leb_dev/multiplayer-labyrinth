﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using GooglePlayGames;
using System.Text;
using GooglePlayGames.BasicApi.Multiplayer;
using GooglePlayGames.BasicApi;
using System.Collections.Generic;
using UnityEngine.Advertisements;

public class Menu_Navigation : MonoBehaviour
{

    private GameObject AskIfReadyToggle;
    private Transform Horizon;
    private float Turns;
    private bool Up;

    void OnGUI()
    {
        if (Online.mIncomingInvitation != null)
        {
            // show the popup
            string who = (Online.mIncomingInvitation.Inviter != null &&
                Online.mIncomingInvitation.Inviter.DisplayName != null) ?
                    Online.mIncomingInvitation.Inviter.DisplayName : (LD.Ru ? "Кто-то" : "Someone");
            GUI.Label(Online.IncomingMatchBannerBox, who + (LD.Ru ? " приглашает вас на матч!" : " is challenging you to a match!"));
            if (GUI.Button(Online.IncomingMatchAcceptButton, (LD.Ru ? "Принять!" : "Accept!")))
            {
                // user wants to accept the invitation!
                PlayGamesPlatform.Instance.TurnBased.AcceptInvitation(
                    Online.mIncomingInvitation.InvitationId, Online.OnMatchStarted);
            }
            if (GUI.Button(Online.IncomingMatchDeclineButton, (LD.Ru ? "Отклонить" : "Decline")))
            {
                // user wants to decline the invitation
                PlayGamesPlatform.Instance.TurnBased.DeclineInvitation(
                    Online.mIncomingInvitation.InvitationId);
            }
        }

        if (Online.mIncomingMatch != null)
        {
            GUI.Box(Online.IncomingMatchBannerBox, (LD.Ru ? "Ваш ход в матче против " : "It's your turn in match against ") + Online.mIncomingMatch.PendingParticipantId + "!");
            // show banner saying ("It's your turn against [opponent]!") and
            // put show buttons for "play" and "not now".
            if (GUI.Button(Online.IncomingMatchAcceptButton, (LD.Ru ? "Принять!" : "Accept!")))
            {
                // play now!
                Online.OnMatchStarted(true, Online.mIncomingMatch);
                Online.mIncomingMatch = null;
            }
            else if (GUI.Button(Online.IncomingMatchDeclineButton, (LD.Ru ? "Отклонить" : "Decline")))
            {
                // stop showing banner:
                Online.mIncomingMatch = null;
            }
        }
    }

    private void Start()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("PreSetUp"))
        {
            SignIn();
            if (Advertisement.isSupported)
            {
                Advertisement.Initialize("1338535", true);
            }
        }

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Victory"))
        {
            LD.Authorized = PlayGamesPlatform.Instance.IsAuthenticated();
            GameObject.Find("SignInButton").GetComponent<Button>().interactable = !LD.Authorized;
            GameObject.Find("SubmitButton").GetComponent<Button>().interactable = LD.Authorized;

            if (LD.Ru)
            {
                GameObject.Find("BackToMenuText").GetComponent<Text>().text = "Назад в меню";
                GameObject.Find("SubmitText").GetComponent<Text>().text = "Отправить результат";
                GameObject.Find("SignInText").GetComponent<Text>().text = "Войти";
            }

            if (LD.Authorized)
            {
                if (LD.GameModeTutorial)
                {
                    Social.ReportProgress(GPGSIds.achievement_ready_for_playing, 100.0f, null);
                }
                else
                {
                    switch (LD.m)
                    {
                        case 5:
                            Social.ReportProgress(GPGSIds.achievement_easy_round, 100.0f, null);
                            break;
                        case 10:
                            Social.ReportProgress(GPGSIds.achievement_thats_a_harder_one, 100.0f, null);
                            break;
                        case 20:
                            Social.ReportProgress(GPGSIds.achievement_hard_as_a_rock, 100.0f, null);
                            break;
                        case 40:
                            Social.ReportProgress(GPGSIds.achievement_psycho, 100.0f, null);
                            break;
                        default:
                            break;
                    }
                }
            }
            if (LD.match == null)
            {
                Save_Manager.DeleteSave();
                if (!LD.GameModeCoop)
                {
                    GameObject.Find("CongratulationText").GetComponent<Text>().color = LD.PlayerColors[LD.CurrentPlayer - 1];
                    if (LD.Ru)
                    {
                        GameObject.Find("CongratulationText").GetComponent<Text>().text = "Поздравляю, игрок " +
                        LD.CurrentPlayer + "! Вы выиграли!";
                    }
                    else
                    {
                        GameObject.Find("CongratulationText").GetComponent<Text>().text = "Congratulations, player " +
                        LD.CurrentPlayer + "! You won that competition!";
                    }
                }
                else
                {
                    if (LD.Ru)
                    {
                        GameObject.Find("CongratulationText").GetComponent<Text>().text = "Победа! Вы вышли из лабиринта!";
                    }
                    else
                    {
                        GameObject.Find("CongratulationText").GetComponent<Text>().text = "Victory! You escaped the labyrinth!";
                    }
                }
                Turns = 0.0f;
                if (!LD.GameModeCoop)
                    Turns = Player_Movement.Turns[LD.CurrentPlayer - 1];
                else
                    for (int i = 0; i < LD.NumberOfPlayers; i++)
                        Turns += Player_Movement.Turns[i];
                if (LD.Ru)
                {
                    GameObject.Find("TurnsText").GetComponent<Text>().text = "За " + Turns + " ходов";
                }
                else
                {
                    GameObject.Find("TurnsText").GetComponent<Text>().text = "In " + Turns + " turns";
                }
            }
            else
            {
                if (LD.match.Status != TurnBasedMatch.MatchStatus.Complete)
                {
                    GameObject.Find("CongratulationText").GetComponent<Text>().color = LD.PlayerColors[LD.CurrentPlayer - 1];
                    if (LD.Ru)
                    {
                        GameObject.Find("CongratulationText").GetComponent<Text>().text = "Поздравляю, игрок " +
                        LD.CurrentPlayer + "! Вы выиграли!";
                    }
                    else
                    {
                        GameObject.Find("CongratulationText").GetComponent<Text>().text = "Congratulations, player " +
                        LD.CurrentPlayer + "! You won that competition!";
                    }
                    Turns = 0.0f;
                    Turns = Player_Movement.Turns[LD.CurrentPlayer - 1];
                    if (LD.Ru)
                    {
                        GameObject.Find("TurnsText").GetComponent<Text>().text = "За " + Turns + " ходов";
                    }
                    else
                    {
                        GameObject.Find("TurnsText").GetComponent<Text>().text = "In " + Turns + " turns";
                    }

                    byte[] FinalData; // match data representing the final state of the match
                    GameState GS = new GameState(true);
                    string FinalDataString = GS.SaveToString();
                    FinalData = new byte[Encoding.Default.GetByteCount(FinalDataString)];
                    FinalData = Encoding.Default.GetBytes(FinalDataString);

                    // define the match's outcome
                    MatchOutcome Outcome = new MatchOutcome();
                    for (int i = 0; i < LD.match.Participants.Count; i++)
                    {
                        // decide if participant p has won, lost or tied, and
                        // their ranking (1st, 2nd, 3rd, ...):
                        MatchOutcome.ParticipantResult result;
                        if (i == (LD.CurrentPlayer - 1))
                        {
                            result = MatchOutcome.ParticipantResult.Win;
                        }
                        else
                        {
                            result = MatchOutcome.ParticipantResult.Loss;
                        }
                        Outcome.SetParticipantResult(LD.match.Participants[i].ParticipantId, result);
                    }

                    // finish the match
                    PlayGamesPlatform.Instance.TurnBased.Finish(LD.match, FinalData, Outcome, (bool success) =>
                    {
                        //todolist: заполнить
                        if (success)
                        {
                            GameObject.Find("SubmitButton").GetComponent<Button>().interactable = LD.Authorized;
                        }
                        else
                        {
                            // an error occurred
                        }
                    });
                }
                else
                {
                    GameObject.Find("CongratulationText").GetComponent<Text>().color = LD.PlayerColors[LD.CurrentPlayer - 1];
                    if (LD.Ru)
                    {
                        GameObject.Find("CongratulationText").GetComponent<Text>().text = "Игрок " +
                        LD.CurrentPlayer + " победил";
                    }
                    else
                    {
                        GameObject.Find("CongratulationText").GetComponent<Text>().text = "Player " +
                        LD.CurrentPlayer + " won that competition";
                    }
                    Turns = 0.0f;
                    Turns = Player_Movement.Turns[LD.CurrentPlayer - 1];
                    if (LD.Ru)
                    {
                        GameObject.Find("TurnsText").GetComponent<Text>().text = "За " + Turns + " ходов";
                    }
                    else
                    {
                        GameObject.Find("TurnsText").GetComponent<Text>().text = "In " + Turns + " turns";
                    }
                }
            }
        }
        
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("MainMenu"))
        {
            Horizon = GameObject.Find("Horizon").GetComponent<Transform>();
            Horizon.localScale = new Vector3(Screen.width / 6f, Screen.height / 3.6f, 1);
            Horizon.localPosition = new Vector3(0, Screen.height/10, 0);
            Up = true;

            if (LD.Ru)
            {
                GameObject.Find("MenuText").GetComponent<Text>().text = "Все на борт!";
                GameObject.Find("StartOnlineButtonText").GetComponent<Text>().text = "Играть\n(Онлайн)";
                GameObject.Find("StartButtonText").GetComponent<Text>().text = "Играть\n(Одиночная / Хотсит)";
                GameObject.Find("AboutButtonText").GetComponent<Text>().text = "Об авторе";
                GameObject.Find("ExitButtonText").GetComponent<Text>().text = "Выйти";
                GameObject.Find("TutorialButtonText").GetComponent<Text>().text = "Обучение";
                GameObject.Find("SignInText").GetComponent<Text>().text = "Войти";
                GameObject.Find("AchievementsText").GetComponent<Text>().text = "Достижения";
                GameObject.Find("LeaderboardsText").GetComponent<Text>().text = "Списки лидеров";
            }
            LD.Authorized = PlayGamesPlatform.Instance.IsAuthenticated();
            GameObject.Find("StartOnlineButton").GetComponent<Button>().interactable = LD.Authorized;
            GameObject.Find("SignInButton").GetComponent<Button>().interactable = !LD.Authorized;
            GameObject.Find("AchievementsButton").GetComponent<Button>().interactable = LD.Authorized;
            GameObject.Find("LeaderboardsButton").GetComponent<Button>().interactable = LD.Authorized;
        }

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("SetUp"))
        {
            if (LD.Ru)
            {
                List<Dropdown.OptionData> PlayersDropdownOptions = new List<Dropdown.OptionData>();
                PlayersDropdownOptions.Add(new Dropdown.OptionData("1 игрок"));
                PlayersDropdownOptions.Add(new Dropdown.OptionData("2 игрокa"));
                PlayersDropdownOptions.Add(new Dropdown.OptionData("3 игрокa"));
                PlayersDropdownOptions.Add(new Dropdown.OptionData("4 игрокa"));
                List<Dropdown.OptionData> ModeDropdownOptions = new List<Dropdown.OptionData>();
                ModeDropdownOptions.Add(new Dropdown.OptionData("Совместный"));
                ModeDropdownOptions.Add(new Dropdown.OptionData("Противостояние (один лабиринт)"));
                ModeDropdownOptions.Add(new Dropdown.OptionData("Противостояние (разные лабиринты)"));
                List<Dropdown.OptionData> DifficultyDropdownOptions = new List<Dropdown.OptionData>();
                DifficultyDropdownOptions.Add(new Dropdown.OptionData("Легко 5x5"));
                DifficultyDropdownOptions.Add(new Dropdown.OptionData("Нормально 10x10"));
                DifficultyDropdownOptions.Add(new Dropdown.OptionData("Сложно 20x20"));
                DifficultyDropdownOptions.Add(new Dropdown.OptionData("Экстрим 40x40"));
                GameObject.Find("BackButtonText").GetComponent<Text>().text = "Назад";
                GameObject.Find("SetUpText").GetComponent<Text>().text = "Настроить игру";
                GameObject.Find("PlayersText").GetComponent<Text>().text = "Количество игроков";
                GameObject.Find("ModeText").GetComponent<Text>().text = "Режим";
                GameObject.Find("DifficultyText").GetComponent<Text>().text = "Сложность";
                GameObject.Find("StartNewButtonText").GetComponent<Text>().text = "Новая игра";
                GameObject.Find("ContinueButtonText").GetComponent<Text>().text = "Продолжить";
                GameObject.Find("PlayersDropdownLabel").GetComponent<Text>().text = "1 игрок";
                GameObject.Find("PlayersDropdown").GetComponent<Dropdown>().options = PlayersDropdownOptions;
                GameObject.Find("ModeDropdownLabel").GetComponent<Text>().text = "Совместный";
                GameObject.Find("ModeDropdown").GetComponent<Dropdown>().options = ModeDropdownOptions;
                GameObject.Find("DifficultyDropdownLabel").GetComponent<Text>().text = "Нормально 10x10";
                GameObject.Find("DifficultyDropdown").GetComponent<Dropdown>().options = DifficultyDropdownOptions;
                GameObject.Find("AskIfReadyToggleLabel").GetComponent<Text>().text = "Запрашивать готовность игрока перед каждым ходом";
            }
            AskIfReadyToggle = GameObject.Find("AskIfReadyToggle");
            AskIfReadyToggle.SetActive(false);
            LD.NumberOfPlayers = 1;
            GameObject.Find("ContinueButton").GetComponent<Button>().interactable = (PlayerPrefs.HasKey("GameState"));
        }

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("OnlineRoom"))
        {
            LD.NumberOfPlayers = 2;
            if (LD.Ru)
            {
                List<Dropdown.OptionData> PlayersDropdownOptions = new List<Dropdown.OptionData>();
                PlayersDropdownOptions.Add(new Dropdown.OptionData("2 игрокa"));
                PlayersDropdownOptions.Add(new Dropdown.OptionData("3 игрокa"));
                PlayersDropdownOptions.Add(new Dropdown.OptionData("4 игрокa"));
                List<Dropdown.OptionData> ModeDropdownOptions = new List<Dropdown.OptionData>();
                ModeDropdownOptions.Add(new Dropdown.OptionData("Противостояние (один лабиринт)"));
                ModeDropdownOptions.Add(new Dropdown.OptionData("Противостояние (разные лабиринты)"));
                List<Dropdown.OptionData> DifficultyDropdownOptions = new List<Dropdown.OptionData>();
                DifficultyDropdownOptions.Add(new Dropdown.OptionData("Легко 5x5"));
                DifficultyDropdownOptions.Add(new Dropdown.OptionData("Нормально 10x10"));
                DifficultyDropdownOptions.Add(new Dropdown.OptionData("Сложно 20x20"));
                DifficultyDropdownOptions.Add(new Dropdown.OptionData("Экстрим 40x40"));
                GameObject.Find("BackButtonText").GetComponent<Text>().text = "Назад";
                GameObject.Find("OnlineRoomText").GetComponent<Text>().text = "Онлайн режим";
                GameObject.Find("PlayersText").GetComponent<Text>().text = "Количество игроков";
                GameObject.Find("ModeText").GetComponent<Text>().text = "Режим";
                GameObject.Find("DifficultyText").GetComponent<Text>().text = "Сложность";
                GameObject.Find("PlayersDropdownLabel").GetComponent<Text>().text = "2 игрока";
                GameObject.Find("ModeDropdownLabel").GetComponent<Text>().text = "Противостояние (один лабиринт)";
                GameObject.Find("DifficultyDropdownLabel").GetComponent<Text>().text = "Нормально 10x10";
                GameObject.Find("InviteFriendsButtonText").GetComponent<Text>().text = "Пригласить друзей";
                GameObject.Find("AcceptInvitationButtonText").GetComponent<Text>().text = "Принять приглашение / Продолжить матч";
                GameObject.Find("QuickMatchButtonText").GetComponent<Text>().text = "Случайный матч";
                GameObject.Find("PlayersDropdown").GetComponent<Dropdown>().options = PlayersDropdownOptions;
                GameObject.Find("ModeDropdown").GetComponent<Dropdown>().options = ModeDropdownOptions;
                GameObject.Find("DifficultyDropdown").GetComponent<Dropdown>().options = DifficultyDropdownOptions;
            }
        }

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Game"))
        {
            if (LD.Ru)
            {
                GameObject.Find("BackButtonText").GetComponent<Text>().text = "Назад в меню";
                GameObject.Find("TurnCounter").GetComponent<Text>().text = "Ходов: 0";
            }
        }

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("About"))
        {
            if (LD.Ru)
            {
                GameObject.Find("BackButtonText").GetComponent<Text>().text = "Назад";
                GameObject.Find("AboutText1").GetComponent<Text>().text = "Разработчик: CyberHatter";
                GameObject.Find("AboutText2").GetComponent<Text>().text = "Особые благодрности:\nSweet_donut\n}{un\nCatoration";
                GameObject.Find("AboutText3").GetComponent<Text>().text = "Надеюсь, вам понравилось";
                GameObject.Find("AboutText4").GetComponent<Text>().text = "Поддержать разработчика или сообщить о проблеме: (Paypal:) mihasik.m@mail.ru";
            }
        }

        StartCoroutine(WaitForReturn());
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene()==SceneManager.GetSceneByName("MainMenu"))
        {
            if (Up)
            {
                if (Horizon.localPosition.y < Screen.height / 3.5f)
                {
                    Horizon.localPosition = new Vector3(Horizon.localPosition.x,
                        Horizon.localPosition.y + (Time.deltaTime * 0.7f * (Screen.height / 3.5f - Horizon.localPosition.y) + 0.4f), Horizon.localPosition.z);
                }
                else
                {
                    Up = false;
                }
            }
            else
            {
                if (Horizon.localPosition.y > Screen.height / 10)
                {
                    Horizon.localPosition = new Vector3(Horizon.localPosition.x,
                        Horizon.localPosition.y - (Time.deltaTime * 0.7f * (Horizon.localPosition.y - Screen.height / 10) + 0.4f), Horizon.localPosition.z);
                }
                else
                {
                    Up = true;
                }
            }
        }
    }

    public void SignIn()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
           // registers a callback to handle game invitations.
           .WithInvitationDelegate(Online.OnInvitationReceived)

           // registers a callback for turn based match notifications.
           .WithMatchDelegate(Online.OnGotMatch)

           .Build();

        PlayGamesPlatform.InitializeInstance(config);

        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();

        Social.localUser.Authenticate((bool Success) =>
        {
            if (Success)
            {
                LD.Authorized = true;
                if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("MainMenu"))
                {
                    GameObject.Find("StartOnlineButton").GetComponent<Button>().interactable = true;
                    GameObject.Find("SignInButton").GetComponent<Button>().interactable = false;
                    GameObject.Find("AchievementsButton").GetComponent<Button>().interactable = true;
                    GameObject.Find("LeaderboardsButton").GetComponent<Button>().interactable = true;

                    AudioClip Bell = Resources.Load<AudioClip>("Sounds/Bell2");
                    GameObject.Find("MenuCamera").GetComponents<AudioSource>()[1].clip = Bell;
                    GameObject.Find("MenuCamera").GetComponents<AudioSource>()[1].Play();
                    Resources.UnloadUnusedAssets();
                }

                if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Victory"))
                {
                    GameObject.Find("SignInButton").GetComponent<Button>().interactable = false;
                    GameObject.Find("SubmitButton").GetComponent<Button>().interactable = true;

                    AudioClip Bell = Resources.Load<AudioClip>("Sounds/Bell2");
                    GameObject.Find("VictoryCamera").GetComponent<AudioSource>().clip = Bell;
                    GameObject.Find("VictoryCamera").GetComponent<AudioSource>().Play();
                    Resources.UnloadUnusedAssets();
                }
            }
        });
    }

    public void SubmitScore()
    {
        SoundControlling.PlaySoundEffect("ButtonClick");

        if (Turns > 10000)
        {
            Turns = 10000;
        }
        string Leaderboard;
        switch (LD.m)
        {
            case 5:
                Leaderboard = GPGSIds.leaderboard_top_players_easy;
                break;
            case 10:
                Leaderboard = GPGSIds.leaderboard_top_players_normal;
                break;
            case 20:
                Leaderboard = GPGSIds.leaderboard_top_players_hard;
                break;
            case 40:
                Leaderboard = GPGSIds.leaderboard_top_players_extreme;
                break;
            default:
                Leaderboard = GPGSIds.leaderboard_top_players_normal;
                break;
        }
        // post score 12345 to leaderboard ID "Cfji293fjsie_QA")
        Social.ReportScore(Mathf.CeilToInt(Turns), Leaderboard, (bool success) => {
            GameObject.Find("SubmitButton").GetComponent<Button>().interactable = false;
            if (LD.Ru)
            {
                GameObject.Find("SubmitButton").GetComponentInChildren<Text>().text = "Успешно отправлено!";
            }
            else
            {
                GameObject.Find("SubmitButton").GetComponentInChildren<Text>().text = "Successfully submitted!";
            }
        });
    }

    public void ShowAchievements()
    {
        SoundControlling.PlaySoundEffect("ButtonClick");

        // show achievements UI
        Social.ShowAchievementsUI();
    }

    public void ShowLeaderboards()
    {
        SoundControlling.PlaySoundEffect("ButtonClick");

        // show leaderboard UI
        Social.ShowLeaderboardUI();
    }

    public void LanguageChosen(bool Ru)
    {
        SoundControlling.PlaySoundEffect("ButtonClick");

        LD.Ru = Ru;
        StartCoroutine(WaitAndGoTo(0.5f, "MainMenu"));
    }

    IEnumerator WaitForReturn()
    {
        while (true)
        {
            if (Input.GetAxis("Cancel") > 0)
            {
                if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("MainMenu"))
                {
                    App_Ex();
                }
                else
                {
                    if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName("Game"))
                    {
                        SceneManager.LoadSceneAsync("MainMenu");
                    }
                    else
                    {
                        if (GameObject.Find("BackButton").GetComponent<Button>().interactable)
                        {
                            SceneManager.LoadSceneAsync("MainMenu");
                        }
                    }
                }
            }
            yield return null;
        }
    }

    public void Back_Click()
    {
        SoundControlling.PlaySoundEffect("ButtonClick");

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Victory"))
        {
            if (Advertisement.IsReady())
                Advertisement.Show();
            GameObject.Find("BackToMenuButton").GetComponent<Button>().interactable = false;
            StartCoroutine(WaitForAdToEnd());
        }
        else
        {
            StartCoroutine(WaitAndGoTo(0.5f, "MainMenu"));
        }
    }

    private IEnumerator WaitForAdToEnd()
    {
        yield return new WaitForSeconds(0.3f);
        while (true)
        {
            if (!Advertisement.isShowing)
            {
                SceneManager.LoadSceneAsync("MainMenu");
                yield break;
            }
            yield return null;
        }
    }

    public void StartClicked()
    {
        SoundControlling.PlaySoundEffect("Bell");

        StartCoroutine(WaitAndGoTo(1f,"SetUp"));
    }

    public void StartOnlineClicked()
    {
        SoundControlling.PlaySoundEffect("Bell");

        StartCoroutine(WaitAndGoTo(1f,"OnlineRoom"));
    }

    public void About_Click()
    {
        SoundControlling.PlaySoundEffect("Steps");

        StartCoroutine(WaitAndGoTo(0.9f,"About"));
    }

    public void App_Ex()
    {
        SoundControlling.PlaySoundEffect("ButtonClick");

        StartCoroutine(WaitAndGoTo(0.5f,"Quit"));
    }

    public void Tutorial_Start()
    {
        LD.match = null;

        LD.NumberOfPlayers = 1;
        LD.GameModeCoop = true;
        LD.GameModeOneLabyrinth = true;
        LD.GameModeTutorial = true;
        LD.m = 5;
        LD.n = 5;
        LD.pt = 2;
        LD.grn = 1;
        LD.flstr = 1;
        LD.tr = 1;
        LD.GameLoading = false;

        SoundControlling.PlaySoundEffect("Bell2");

        StartCoroutine(WaitAndGoTo(1f, "Game"));
    }

    public void PlaySoundEffect(string Effect)
    {
        SoundControlling.PlaySoundEffect(Effect);
    }
    
    IEnumerator WaitAndGoTo(float Wait4, string Room)
    {
        yield return new WaitForSeconds(Wait4);
        if (Room=="Quit")
        {
            //Выход из приложения
            Application.Quit();
        }
        else
        {
            SceneManager.LoadSceneAsync(Room);
        }
        yield break;
    }

    public void PlayersOptionChanged()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("SetUp"))
        {
            if (GameObject.Find("PlayersDropdown").GetComponent<Dropdown>().value != 0)
            {
                GameObject.Find("ModeDropdown").GetComponent<Dropdown>().interactable = true;
                AskIfReadyToggle.SetActive(true);
            }
            else
            {
                GameObject.Find("ModeDropdown").GetComponent<Dropdown>().interactable = false;
                AskIfReadyToggle.SetActive(false);
            }
        }
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("OnlineRoom"))
        {
            LD.NumberOfPlayers = GameObject.Find("PlayersDropdown").GetComponent<Dropdown>().value + 2;
        }
    }

    public void Start_New_Click(bool NewGame)
    {
        LD.match = null;

        int ModeValue = GameObject.Find("ModeDropdown").GetComponent<Dropdown>().value,
            DifficultyValue = GameObject.Find("DifficultyDropdown").GetComponent<Dropdown>().value,
            PlayersValue = GameObject.Find("PlayersDropdown").GetComponent<Dropdown>().value;


        if (NewGame)
        {
            SoundControlling.PlaySoundEffect("Bell");
        }
        else
        {
            SoundControlling.PlaySoundEffect("Bell2");
        }

        if (AskIfReadyToggle.activeSelf)
        {
            LD.AskIfReady = AskIfReadyToggle.GetComponent<Toggle>().isOn;
        }

        if (NewGame)
        {
            LD.GameLoading = false;
            Save_Manager.DeleteSave();

            LD.GameModeTutorial = false;
            LD.NumberOfPlayers = PlayersValue + 1;
            if (LD.NumberOfPlayers > 1)
                if (ModeValue == 0)
                {
                    LD.GameModeCoop = true;
                    LD.GameModeOneLabyrinth = true;
                }
                else
                {
                    LD.GameModeCoop = false;
                    if (ModeValue == 2)
                        LD.GameModeOneLabyrinth = false;
                    else
                        LD.GameModeOneLabyrinth = true;
                }
            //Найти выпадающий список сложности, запросить значение и установить размеры лабиринта с количеством ям
            switch (DifficultyValue)
            {
                case 0:
                    LD.m = 5;
                    LD.n = 5;
                    LD.pt = 2;
                    LD.grn = 1;
                    LD.flstr = 1;
                    LD.tr = 1;
                    break;
                case 1:
                    LD.m = 10;
                    LD.n = 10;
                    LD.pt = 4;
                    LD.grn = 2;
                    LD.flstr = 3;
                    LD.tr = 1;
                    break;
                case 2:
                    LD.m = 20;
                    LD.n = 20;
                    LD.pt = 12;
                    LD.grn = 6;
                    LD.flstr = 7;
                    LD.tr = 2;
                    break;
                case 3:
                    LD.m = 40;
                    LD.n = 40;
                    LD.pt = 36;
                    LD.grn = 18;
                    LD.flstr = 15;
                    LD.tr = 4;
                    break;
            }
        }
        else
        {
            LD.GameLoading = true;
            Save_Manager.LoadGame();
        }

        StartCoroutine(WaitAndGoTo(0.7f, "Game"));
    }

}