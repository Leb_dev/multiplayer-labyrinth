﻿using UnityEngine;

public class SeaAnimationControl : MonoBehaviour
{
    private Transform Sea, Sea2;
    private bool SeaMoving = true, Sea2Moving = false;
    
    void Start()
    {
        Sea = GameObject.Find("Sea").GetComponent<Transform>();
        Sea2 = GameObject.Find("Sea2").GetComponent<Transform>();
        Sea.localScale = new Vector3(Screen.width / 3.2f, Screen.height / 2.4f, 1);
        Sea2.localScale = new Vector3(Screen.width / 3.2f, Screen.height / 2.4f, 1);
    }
    
    void Update ()
    {
        if (Sea.localPosition.x>=695)
        {
            Sea2Moving = true;
        }
        if (Sea2.localPosition.x>=695)
        {
            SeaMoving = true;
        }
        if (Sea.localPosition.x >= 1973)
        {
            Sea.localPosition = new Vector3(-1973, Sea.localPosition.y, 0);
            SeaMoving = false;
        }
        if (Sea2.localPosition.x >= 1973)
        {
            Sea2.localPosition = new Vector3(-1973, Sea2.localPosition.y, 0);
            Sea2Moving = false;
        }
        if (SeaMoving)
        {
            Sea.localPosition = new Vector3(Sea.localPosition.x+Time.deltaTime*15, Sea.localPosition.y, 0);
        }
        if (Sea2Moving)
        {
            Sea2.localPosition = new Vector3(Sea2.localPosition.x + Time.deltaTime*15, Sea2.localPosition.y, 0);
        }
	}

}