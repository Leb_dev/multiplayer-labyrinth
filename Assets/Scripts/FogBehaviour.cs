﻿using System;
using System.Collections;
using UnityEngine;

public class FogBehaviour : MonoBehaviour {

    private bool LoopStarted = false, Right = true;
    Vector3 Step;
     
	void Start ()
    {
        LoopStarted = false;
        StartCoroutine(Cycle());
	}

    private IEnumerator Cycle()
    {
        yield return new WaitForSeconds(LD.Seeder.Next(0,2000)/100f);
        while (true)
        {
            if (!LoopStarted)
            {
                Sprite FogSprite = Resources.Load<Sprite>("Sprites/Fog/Fog"+LD.Seeder.Next(1,14));
                if (LD.Seeder.Next(1, 3) == 1)
                {
                    gameObject.transform.localPosition = new Vector3(-Screen.width, LD.Seeder.Next(-Screen.height/2, Screen.height/2 + 1), 0);
                    Right = true;
                }
                else
                {
                    gameObject.transform.localPosition = new Vector3(Screen.width, LD.Seeder.Next(-Screen.height/2, Screen.height/2 + 1), 0);
                    Right = false;
                }
                gameObject.GetComponent<SpriteRenderer>().sprite = FogSprite;
                gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, LD.Seeder.Next(30, 100) / 100f);
                float Scale = LD.Seeder.Next(10, Screen.height / 20) / 10f;
                gameObject.transform.localScale = new Vector3(Scale, Scale, 0);
                float XStep = Screen.width / (float)LD.Seeder.Next(900, 2000);
                Step = new Vector3(XStep, 0, 0);
                LoopStarted = true;
                Resources.UnloadUnusedAssets();
            }
            else
            {
                if (Right)
                {
                    if (gameObject.transform.localPosition.x < Screen.width)
                    {
                        gameObject.transform.localPosition += Step;
                    }
                    else
                    {
                        yield return new WaitForSeconds(LD.Seeder.Next(0, 5000) / 1000f);
                        LoopStarted = false;
                    }
                }
                else
                {
                    if (gameObject.transform.localPosition.x > -Screen.width)
                    {
                        gameObject.transform.localPosition -= Step;
                    }
                    else
                    {
                        yield return new WaitForSeconds(LD.Seeder.Next(0, 5000) / 1000f);
                        LoopStarted = false;
                    }
                }
            }
            yield return null;
        }
    }

}
