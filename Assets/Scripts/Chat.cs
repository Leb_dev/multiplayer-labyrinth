﻿using UnityEngine;
using UnityEngine.UI;

public class Chat : MonoBehaviour
{

    private static int BottomTextNumber = 0;
    private static Button UpConsoleButton, DownConsoleButton;

    void Start()
    {
        UpConsoleButton = GameObject.Find("UpConsoleButton").GetComponent<Button>();
        DownConsoleButton = GameObject.Find("DownConsoleButton").GetComponent<Button>();
        if (!LD.GameLoading)
            for (int i = 0; i < LD.MaxChatTexts; i++)
            {
                LD.ChatTexts[i] = "";
                LD.ChatTextsColors[i] = Color.white;
            }
        else
        {
            for (int i = 0; i < LD.MaxChatTexts; i++)
            {
                LD.ChatTextsColors[i] = Color.black;
            }
            UpdateConsoleTexts();
            UpConsoleButton.interactable = !(LD.ChatTexts[LD.MaxVisibleChatTexts] == "");
        }
    }

    public void NavigateConsole(bool Up)
    {
        if (Up)
        {
            BottomTextNumber++;
            UpdateConsoleTexts();
            if (BottomTextNumber == LD.MaxChatTexts - LD.MaxVisibleChatTexts)
            {
                UpConsoleButton.interactable = false;
            }
            else
            {
                if (LD.ChatTexts[BottomTextNumber+LD.MaxVisibleChatTexts] == "")
                {
                    UpConsoleButton.interactable = false;
                }
            }
            DownConsoleButton.interactable = true;
        }
        else
        {
            BottomTextNumber--;
            UpdateConsoleTexts();
            if (BottomTextNumber == 0)
            {
                DownConsoleButton.interactable = false;
            }
            UpConsoleButton.interactable = true;
        }
    }

    private static void UpdateConsoleTexts()
    {
        for (int i = 0; i < LD.MaxVisibleChatTexts; i++)
        {
            GameObject.Find("Chat_Text_" + (i + 1)).GetComponent<Text>().text = LD.ChatTexts[BottomTextNumber + i];
            GameObject.Find("Chat_Text_" + (i + 1)).GetComponent<Text>().color = LD.ChatTextsColors[BottomTextNumber + i];
        }
    }

    public void RequestPosition()
    {
        if (!Player_Movement.IsActionActive && Player_Movement.CanPlay)
        {
            Player_Movement.IsActionActive = true;
            if (LD.Ru)
            {
                switch (LD.Seeder.Next(1, 4))
                {
                    case 1:
                        AddNewText("Игрок " + LD.CurrentPlayer + ": \"Мне кажется или я здесь уже был?\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                        break;
                    case 2:
                        AddNewText("Игрок " + LD.CurrentPlayer + ": \"Это место кажется мне знакомым...\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                        break;
                    case 3:
                        AddNewText("Игрок " + LD.CurrentPlayer + ": \"Разве я здесь не проходил?\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                        break;
                }
            }
            else
            {
                switch (LD.Seeder.Next(1, 4))
                {
                    case 1:
                        AddNewText("Player " + LD.CurrentPlayer + ": \"Have I already been here?\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                        break;
                    case 2:
                        AddNewText("Player " + LD.CurrentPlayer + ": \"This place seems familliar to me...\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                        break;
                    case 3:
                        AddNewText("Player " + LD.CurrentPlayer + ": \"Have I ever visited this place?\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                        break;
                }
            }
            if (Player_Movement.AskIfVisited())
            {
                SoundControlling.PlaySoundEffect("RequestSuccess");

                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 5))
                    {
                        case 1:
                            AddNewText("Голос в Вашей голове: \"Этот коридор я уже проходил!\"", Color.white);
                            break;
                        case 2:
                            AddNewText("Голос в Вашей голове: \"Да. Обновим карту\".", Color.white);
                            break;
                        case 3:
                            AddNewText("Голос в Вашей голове: \"Точно, я знаю, где я\".", Color.white);
                            break;
                        case 4:
                            AddNewText("Голос в Вашей голове: \"Припоминаю это место\".", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 5))
                    {
                        case 1:
                            AddNewText("A voice in your head: \"Sure!\"", Color.white);
                            break;
                        case 2:
                            AddNewText("A voice in your head: \"Yeah, let's refresh the map\".", Color.white);
                            break;
                        case 3:
                            AddNewText("A voice in your head: \"Yep, now I know where I am\".", Color.white);
                            break;
                        case 4:
                            AddNewText("A voice in your head: \"Piece of cake\".", Color.white);
                            break;
                    }
                }
            }
            else
            {
                SoundControlling.PlaySoundEffect("RequestFailure");

                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Голос в Вашей голове: \"Определённо.. Нет\".", Color.white);
                            break;
                        case 2:
                            AddNewText("Голос в Вашей голове: \"Нет, не вижу ничего похожего на то, что видел раньше\".", Color.white);
                            break;
                        case 3:
                            AddNewText("Голос в Вашей голове: \"Не припоминаю ничего такого\".", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("A voice in your head: \"Certainly... Not\".", Color.white);
                            break;
                        case 2:
                            AddNewText("A voice in your head: \"No, I can't see anything familliar\".", Color.white);
                            break;
                        case 3:
                            AddNewText("A voice in your head: \"Nothing to add to the map I have right now\".", Color.white);
                            break;
                    }
                }
            }
            
            if (LD.NumberOfPlayers > 1)
                Player_Movement.CoroutineToStart = "Turn_Pass";
            else
                Player_Movement.IsActionActive = false;
        }
    }

    public static void Report(string Event, float Value1 = 0, int Value2 = 0)
    {
        switch (Event)
        {
            case "CantDrop":
                if (LD.Ru)
                {
                    AddNewText("Нельзя выбросить предмет не на пустой клетке.", Color.white);
                }
                else
                {
                    AddNewText("Can't drop item while not on an empty tile.", Color.white);
                }
                break;
            case "FalseTreasureBrought":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 5))
                    {
                        case 1:
                            AddNewText("Сокровище было ложным. Оно превращается в пыль, когда игрок " + LD.CurrentPlayer + " выходит из лабиринта.", Color.white);
                            break;
                        case 2:
                            AddNewText("И сокровище, что нёс игрок " + LD.CurrentPlayer + "... Было ложным. В следующий раз повезёт!", Color.white);
                            break;
                        case 3:
                            AddNewText("Вы несли ложное сокровище, игрок " + LD.CurrentPlayer + ". Давайте просто выкинем его.", Color.white);
                            break;
                        case 4:
                            AddNewText("Сокровище неожиданно пропадает из сумки игрока " + LD.CurrentPlayer + ". Без сомнений, это было ложное.", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 5))
                    {
                        case 1:
                            AddNewText("The treasure was a false one. It turns into ashes when player " + LD.CurrentPlayer + " reaches the exit.", Color.white);
                            break;
                        case 2:
                            AddNewText("And the treasure player " + LD.CurrentPlayer + " carried... Was a false one. Better luck next time!", Color.white);
                            break;
                        case 3:
                            AddNewText("You were carrying a false treasure, player " + LD.CurrentPlayer + ". Let's just throw it away.", Color.white);
                            break;
                        case 4:
                            AddNewText("The treasure suddenly disappears from " + LD.CurrentPlayer + " player's bag. It was a false one, no doubt.", Color.white);
                            break;
                    }
                }
                break;
            case "PitExitFound":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Что это такое? Выход из водоворота №" + Value1 + "\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Это выход из водоворота №" + Value1 + "\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Табличка: *Выход из водоворота №" + Value1 + "*. Надо запомнить\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"What's that? An exit from the whirlpool №" + Value1 + "\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"I can see the " + Value1 + " whirlpool's exit\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"A sign: *Exit from the whirlpool №" + Value1 + "*. Ok, I should remember that\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                }
                break;
            case "FallingIntoPit":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 5))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Ааааа!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Нееет...\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Водоворот!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 4:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Теряю управление!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 5))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + " попал в водоворот №" + Value1 + ", вроде, цел.", Color.white);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + " угодил в водоворот №" + Value1 + ".", Color.white);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + " нахлебался воды... Попав в водоворот №" + Value1 + ".", Color.white);
                            break;
                        case 4:
                            AddNewText("Игрока " + LD.CurrentPlayer + " затянул водоворот №" + Value1 + "... Он оказался в другом месте.", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 5))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Ouch!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Nooo...\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"A whirlpool!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 4:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Losing control... SOS!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 5))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + " drawned in the whirlpool №" + Value1 + ".", Color.white);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + " was sucked into the whirlpool №" + Value1 + ".", Color.white);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + " choked on water... When got into the whirlpool №" + Value1 + ".", Color.white);
                            break;
                        case 4:
                            AddNewText("Player " + LD.CurrentPlayer + " got into the whirlpool №" + Value1 + "... And found thyself somewhere else.", Color.white);
                            break;
                    }
                }
                break;
            case "SlowDown":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Ай!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Больно, ох!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Это ловушка!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + " попал в ловушку и стал ходить в два раза медленнее.", Color.white);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + " потревожил скрытую ловушку. Ему труднее передвигаться.", Color.white);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + " замедлен из-за попадания в ловушку.", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Ouch!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"That hurts!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"A trap!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + " triggered a trap and became two times slower.", Color.white);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + " triggered a trap. His turns last longer since now.", Color.white);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + " got slowed down because of stepping into a trap.", Color.white);
                            break;
                    }
                }
                break;
            case "SpeedUp":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Да! Так-то лучше\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Скорость, я скорость!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Вот так! Апгрейд\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + " нашёл ускоритель.", Color.white);
                            break;
                        case 2:
                            AddNewText("Скрость игрока " + LD.CurrentPlayer + " увеличина.", Color.white);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + " теперь движется быстрее благодаря найденному бонусу.", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Yeah! That's better\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"I'm speed!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Boost\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + " found a speedup.", Color.white);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + " got his speed increased.", Color.white);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + " now moves faster because of the bonus found.", Color.white);
                            break;
                    }
                }
                break;
            case "StuckInWeb":
                if (LD.Ru)
                {
                    AddNewText("Игрок " + LD.CurrentPlayer + " пойман осьминогом на " + Value1 + " ходов.", Color.white);
                }
                else
                {
                    AddNewText("Player " + LD.CurrentPlayer + " got caught by octopus for " + Value1 + " turn(s).", Color.white);
                }
                break;
            case "StunnedWithDynamite":
                if (LD.Ru)
                {
                    AddNewText("Игрок " + LD.CurrentPlayer + " оглушил игрока " + Value2 + " динамитом на " + Value1 + " ходов.", Color.white);
                }
                else
                {
                    AddNewText("Player " + LD.CurrentPlayer + " stunned player " + Value2 + " with a dynamite for " + Value1 + " turns.", Color.white);
                }
                break;
            case "MatchesLit":
                if (LD.Ru)
                {
                    AddNewText("В тусклом свете фонаря игрок " + LD.CurrentPlayer + " узнаёт немного о местности вокруг.", Color.white);
                    if (Value1 == 1)
                    {
                        AddNewText("Также, замечая, что несёт ложное сокровище, игрок " + LD.CurrentPlayer + " избавляется от него.", Color.white);
                    }
                }
                else
                {
                    AddNewText("In the dim light of the lamp player " + LD.CurrentPlayer + " can distinguish some objects around.", Color.white);
                    if (Value1 == 1)
                    {
                        AddNewText("Also noticing that the treasure in his pocket was a false one, player " + LD.CurrentPlayer + " throws it away.", Color.white);
                    }
                }
                break;
            case "InventoryFull":
                if (LD.Ru)
                {
                    AddNewText("Невозможно подобрать, инвентарь полон.", Color.white);
                }
                else
                {
                    AddNewText("Can't pick it up, while the inventory is full.", Color.white);
                }
                break;
            case "HaveTreasure":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 3))
                    {
                        case 1:
                            AddNewText("Нельзя нести одновременно более одного сокровища.", Color.white);
                            break;
                        case 2:
                            AddNewText("Невозможно нести сразу два сокровища.", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 3))
                    {
                        case 1:
                            AddNewText("Cant't carry two treasures at the same time.", Color.white);
                            break;
                        case 2:
                            AddNewText("Cant't pick up another treasure, you already have one.", Color.white);
                            break;
                    }
                }
                break;
            case "DynamitePickedUp":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Бабах!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Ух, ты! Динамит!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Взрывчатка, берегите уши!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + " нашёл динамит.", Color.white);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + " поднял динамитную шашку.", Color.white);
                            break;
                        case 3:
                            AddNewText("У игрока " + LD.CurrentPlayer + " теперь есть взрывчатка, берегитесь!", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Kaboom!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Wow! Dynamite\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Explosives, lie down!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + " found dynamite.", Color.white);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + " picked up a stick of dynamite .", Color.white);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + " now have some explosives with him. Watch out!", Color.white);
                            break;
                    }
                }
                break;
            case "TreasurePickedUp":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Золотишко!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Отлично!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Так, теперь нужно найти выход из лабиринта.\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 3))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + " подобрал сокровище.", Color.white);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + " нашёл сокровище.", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Goooold!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Nice!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"Ok, now let's get out of here\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 3))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + " picked up a treasure.", Color.white);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + " found a treasure.", Color.white);
                            break;
                    }
                }
                break;
            case "MatchesPickedUp":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Должно быть, с обломков корабля\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Освети мой путь!\"", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + ": \"Отлично сохранился\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 3))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + " нашёл фонарь.", Color.white);
                            break;
                        case 2:
                            AddNewText("Игрок " + LD.CurrentPlayer + " подобрал фонарь.", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"This might be useful\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"A lamp, huh\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + ": \"I'll use it wisely\".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                    switch (LD.Seeder.Next(1, 3))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + " picked up a lamp.", Color.white);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + " found a lamp.", Color.white);
                            break;
                    }
                }
                break;
            case "CantLight":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 3))
                    {
                        case 1:
                            AddNewText("Вне лабиринта нет опасных мест, незачем использовать фонарь.", Color.white);
                            break;
                        case 2:
                            AddNewText("Не стоит тратить фонарь вне лабиринта.", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 3))
                    {
                        case 1:
                            AddNewText("I don't think there is anything dangerous here, no need to use the lamp.", Color.white);
                            break;
                        case 2:
                            AddNewText("I shouldn't light the lamp while not in the labyrinth.", Color.white);
                            break;
                    }
                }
                break;
            case "CantDetonate":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 3))
                    {
                        case 1:
                            AddNewText("Нельзя воспользоваться взрывчаткой за пределами лабиринта.", Color.white);
                            break;
                        case 2:
                            AddNewText("Не на что применить здесь, сначала вернитесь в лабиринт.", Color.white);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 3))
                    {
                        case 1:
                            AddNewText("Nothing to use it for here, return to the labyrinth first.", Color.white);
                            break;
                        case 2:
                            AddNewText("Can't use dynamite while not in the labyrinth.", Color.white);
                            break;
                    }
                }
                break;
            case "TreasureStolen":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + " забрал сокровище игрока " + Value1 + ".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Оглушённый игрок " + Value1 + " выронил сокровище, игрок " + LD.CurrentPlayer + " подбирает его.", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Пока игрок " + Value1 + " оглушён, его сокровище забирает игрок " + LD.CurrentPlayer + ".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + " stole a treasure from player " + Value1 + "'s  inventory.", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Stunned player " + Value1 + " carried a treasure, player " + LD.CurrentPlayer + " stole it.", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Player " + Value1 + "'s treasure was stolen by player " + LD.CurrentPlayer + ".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                }
                break;
            case "CantSteal":
                if (LD.Ru)
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Игрок " + LD.CurrentPlayer + " пытался украсть сокровище игрока " + Value1 + ", но не смог, так как уже несёт одно.", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Также игрок " + LD.CurrentPlayer + " попытался украсть сокровище игрока " + Value1 + ", но, похоже, уже несёт одно в руках.", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Игрок " + LD.CurrentPlayer + " уже несёт сокровище, поэтому не смог украсть его у игрока " + Value1 + ".", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                }
                else
                {
                    switch (LD.Seeder.Next(1, 4))
                    {
                        case 1:
                            AddNewText("Player " + LD.CurrentPlayer + " also tried to steal player " + Value1 + "'s treasure, but it seems player " + LD.CurrentPlayer + " already had one.", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 2:
                            AddNewText("Player " + LD.CurrentPlayer + " tried to steal player " + Value1 + "'s treasure, but he/she already had one.", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                        case 3:
                            AddNewText("Player " + LD.CurrentPlayer + " already has got a treasure so he can't take player " + Value1 + "'s one.", LD.PlayerColors[LD.CurrentPlayer - 1]);
                            break;
                    }
                }
                break;
            case "CaveFound":
                if (LD.Ru)
                {
                    AddNewText("Игрок " + LD.CurrentPlayer + " нашёл путь в лабиринт "+ Value1 +" через пещеру.", Color.white);
                }
                else
                {
                    AddNewText("Player " + LD.CurrentPlayer + " found the way to the " + Value1 + " labyrinth throught the cave.", Color.white);
                }
                break;
            case "CaveEntered":
                if (LD.Ru)
                {
                    AddNewText("Игрок " + LD.CurrentPlayer + " перешёл в лабиринт " + Value1 + " через пещеру.", Color.white);
                }
                else
                {
                    AddNewText("Player " + LD.CurrentPlayer + " moved to the " + Value1 + " labyrinth through the cave.", Color.white);
                }
                break;
            default:
                if (LD.Ru)
                {
                    AddNewText("Что-то пошло не так.", Color.white);
                }
                else
                {
                    AddNewText("Something went wrong.", Color.white);
                }
                break;
        }
    }

    //Добавление нового текста
    public static void AddNewText(string NewText, Color Col)
    {
        if (LD.ChatTexts[0] != NewText)
        {
            //Смещаем элементы
            for (int i = LD.MaxChatTexts - 2; i >= 0; i--)
            {
                LD.ChatTexts[i + 1] = LD.ChatTexts[i];
                LD.ChatTextsColors[i + 1] = LD.ChatTextsColors[i];
            }
            //В первый заносим новый
            LD.ChatTexts[0] = NewText;
            LD.ChatTextsColors[0] = Col;
            //Обновляем текст всех объектов
            BottomTextNumber = 0;
            UpdateConsoleTexts();
            UpConsoleButton.interactable = !(LD.ChatTexts[LD.MaxVisibleChatTexts] == "");
            DownConsoleButton.interactable = false;
            return;
        }
    }

}