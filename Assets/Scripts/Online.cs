﻿using GooglePlayGames;
using GooglePlayGames.BasicApi.Multiplayer;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Online : MonoBehaviour
{
    
    private const int MinOpponents = 1;
    private const int Variant = 0;  // default

    public static Invitation mIncomingInvitation = null;
    public static TurnBasedMatch mIncomingMatch;

    public static Rect IncomingMatchBannerBox = new Rect(Screen.width / 2 - Screen.width / 4, Screen.height / 2 - Screen.height / 4, Screen.width / 2, Screen.height / 2);
    public static Rect IncomingMatchAcceptButton = new Rect(9 * Screen.width / 32, Screen.height / 3, 3 * Screen.width / 16, Screen.height / 3);
    public static Rect IncomingMatchDeclineButton = new Rect(17 * Screen.width / 32, Screen.height / 3, 3 * Screen.width / 16, Screen.height / 3);

    public void QuickMatch()
    {
        PlayGamesPlatform.Instance.TurnBased.CreateQuickMatch(MinOpponents, (uint)LD.NumberOfPlayers - 1,
        Variant, OnMatchStarted);
    }

    public void InviteClicked()
    {
        PlayGamesPlatform.Instance.TurnBased.CreateWithInvitationScreen(MinOpponents, (uint)LD.NumberOfPlayers - 1,
            Variant, OnMatchStarted);
    }

    public void InviteAccept()
    {
        PlayGamesPlatform.Instance.TurnBased.AcceptFromInbox(OnMatchStarted);
    }

    // match delegate:
    public static void OnGotMatch(TurnBasedMatch match, bool ShouldAutoLaunch)
    {
        // if shouldAutoLaunch is false, this means it's not clear that the user
        // wants to jump into the game right away (for example, we might have received
        // this match from a background push notification). So, instead, we will
        // calmly hold on to the match and show a prompt so they can decide
        mIncomingMatch = match;
    }

    // called when an invitation is received:
    public static void OnInvitationReceived(Invitation invitation, bool ShouldAutoAccept)
    {
        // The user has not yet indicated that they want to accept this invitation.
        // We should *not* automatically accept it. Rather we store it and 
        // display an in-game popup:
        mIncomingInvitation = invitation;
    }

    // Callback:
    public static void OnMatchStarted(bool success, TurnBasedMatch match)
    {
        int ModeValue = GameObject.Find("ModeDropdown").GetComponent<Dropdown>().value,
            DifficultyValue = GameObject.Find("DifficultyDropdown").GetComponent<Dropdown>().value,
            PlayersValue = GameObject.Find("PlayersDropdown").GetComponent<Dropdown>().value;

        LD.GameModeTutorial = false;

        if (success)
        {
            LD.match = match;

            // get the match data
            byte[] myData = match.Data;

            // if the match is in the completed state, acknowledge it
            if (match.Status == TurnBasedMatch.MatchStatus.Complete)
            {
                PlayGamesPlatform.Instance.TurnBased.AcknowledgeFinished(match,
                        (bool AcknowledgeSuccess) =>
                        {
                            if (AcknowledgeSuccess)
                            {
                                LD.GameLoading = true;
                                GameState GS = new GameState(false);
                                GS = GS.LoadFromString(Encoding.Default.GetString(myData));
                                GS.LoadValues();
                                SceneManager.LoadSceneAsync("Victory");
                            }
                            else
                            {
                                // show error
                            }
                        });
            }
            else
            {

                // I can only make a move if the match is active and it's my turn!
                Player_Movement.CanPlay = (match.Status == TurnBasedMatch.MatchStatus.Active);

                if (match.TurnStatus != TurnBasedMatch.MatchTurnStatus.MyTurn)
                {
                    return;
                }

                if (myData == null)
                {
                    LD.GameLoading = false;
                    LD.GameModeTutorial = false;
                    LD.NumberOfPlayers = PlayersValue + 2;
                    LD.GameModeCoop = false;
                    if (ModeValue == 0)
                    {
                        LD.GameModeOneLabyrinth = true;
                    }
                    else
                    {
                        LD.GameModeOneLabyrinth = false;
                    }
                    //Найти выпадающий список сложности, запросить значение и установить размеры лабиринта с количеством ям
                    switch (DifficultyValue)
                    {
                        case 0:
                            LD.m = 5;
                            LD.n = 5;
                            LD.pt = 2;
                            LD.grn = 1;
                            LD.flstr = 1;
                            LD.tr = 1;
                            break;
                        case 1:
                            LD.m = 10;
                            LD.n = 10;
                            LD.pt = 4;
                            LD.grn = 2;
                            LD.flstr = 3;
                            LD.tr = 1;
                            break;
                        case 2:
                            LD.m = 20;
                            LD.n = 20;
                            LD.pt = 12;
                            LD.grn = 6;
                            LD.flstr = 7;
                            LD.tr = 2;
                            break;
                        case 3:
                            LD.m = 40;
                            LD.n = 40;
                            LD.pt = 36;
                            LD.grn = 18;
                            LD.flstr = 15;
                            LD.tr = 4;
                            break;
                    }
                }
                else
                {
                    LD.GameLoading = true;
                    GameState GS = new GameState(false);
                    GS = GS.LoadFromString(Encoding.Default.GetString(myData));
                    GS.LoadValues();
                    LD.CurrentPlayer = match.Participants.IndexOf(match.PendingParticipant) + 1;
                }
                SceneManager.LoadSceneAsync("Game");
            }
        }
        else
        {
            // show error message
        }
    }

}