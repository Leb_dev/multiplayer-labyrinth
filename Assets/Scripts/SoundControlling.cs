﻿using UnityEngine;

public class SoundControlling : MonoBehaviour {

    public static void PlaySoundEffect(string Effect)
    {
        AudioClip Sound = Resources.Load<AudioClip>("Sounds/" + Effect);
        GameObject.Find("SoundEffectSource").GetComponent<AudioSource>().clip = Sound;
        GameObject.Find("SoundEffectSource").GetComponent<AudioSource>().Play();
        Resources.UnloadUnusedAssets();
    }

}
