﻿using System.Collections;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{

	private void Start ()
    {
        switch (gameObject.name)
        {
            case "Detonation":
                SoundControlling.PlaySoundEffect("Detonation");
                break;
            case "MineDetonation":
                SoundControlling.PlaySoundEffect("Mine");
                break;
            case "Light":
                SoundControlling.PlaySoundEffect("Light");
                break;
            default:
                break;
        }
        StartCoroutine(WaitAndSelfDestruct());
	}

    private IEnumerator WaitAndSelfDestruct()
    {
        yield return new WaitForSeconds(1.1f);
        Destroy(gameObject);
    }

}
