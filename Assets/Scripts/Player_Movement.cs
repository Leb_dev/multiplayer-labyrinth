﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using System.Text;
using GooglePlayGames;

public class Player_Movement : MonoBehaviour
{

    public enum SwipeDirection { None, Up, Right, Down, Left, In, Out }; //Направления свайпов
    public static SwipeDirection Swipe;

    public static string CoroutineToStart = "Move_Player";   //Корутина, которую выполнять по свайпу

    public enum ObjectType
    {
        FloorTile, Wall, Pit, PitExit, Treasure, Dynamite, Interruption, Spikes, SpeedUp, Matches, Web, Cave
    }
    public struct ObjectsData
    {
        public ObjectType Obj;
        public int x, y;

        public ObjectsData(ObjectType TempObj, int i1, int i2)
        {
            Obj = TempObj;
            x = i1;
            y = i2;
        }
    }
    public static List<ObjectsData>[] SeenObjects = new List<ObjectsData>[LD.MaxNumberOfPlayers];
    public static List<ObjectsData>[] HiddenObjects = new List<ObjectsData>[LD.MaxNumberOfPlayers];

    public static float ZoomOut;
    private float DragDistance;

    public static GameObject SaveButton, WaitingPanel, SubmitButton,
        TutorialPanel, FingerDragFourSides, FingerClickBag, FingerClickClear,
        FingerClickPick, EnterCaveButton;
    public static GameObject[,] FloorTiles, Walls; 

    public static bool IsActionActive = true;
    public static bool CanPlay;

    public static float[] Turns = new float[LD.MaxNumberOfPlayers];

    private byte[] MyData;

    private Camera CameraComponent;

    private Vector2 InitPoint1, InitPoint2;

    //Запуск скрипта
    void Start()
    {
        Swipe = SwipeDirection.None;
        DragDistance = Screen.height / 10;

        CameraComponent = gameObject.GetComponent<Camera>(); //Настройка на объект камеры
        ZoomOut = CameraComponent.orthographicSize; //Сохранение начального размера камеры

        TutorialPanel = GameObject.Find("TutorialPanel");
        SubmitButton = GameObject.Find("SubmitButton");
        WaitingPanel = GameObject.Find("WaitingPanel");
        FingerDragFourSides = GameObject.Find("FingerDragFourSides");
        FingerClickBag = GameObject.Find("FingerClickBag");
        FingerClickClear = GameObject.Find("FingerClickClear");
        FingerClickPick = GameObject.Find("FingerClickPick");
        SaveButton = GameObject.Find("SaveButton");
        EnterCaveButton = GameObject.Find("EnterCaveButton");

        TutorialPanel.SetActive(false);
        WaitingPanel.SetActive(false);
        FingerDragFourSides.SetActive(false);
        FingerClickBag.SetActive(false);
        FingerClickClear.SetActive(false);
        FingerClickPick.SetActive(false);
        EnterCaveButton.SetActive(false);

        if (LD.match == null)
        {
            CanPlay = true;
        }
        else
        {
            SaveButton.SetActive(false);
        }

        if (LD.GameModeTutorial == true)
        {
            Tutorial T = gameObject.AddComponent<Tutorial>();
        }
        else
        {
            IsActionActive = false;
        }

        if (!LD.GameLoading)
        {
            //Инициализация и обнуление списков и массивов
            for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
            {
                SeenObjects[i] = new List<ObjectsData>();
                HiddenObjects[i] = new List<ObjectsData>();
                SeenObjects[i].Clear();
                HiddenObjects[i].Clear();
                LD.TurnOrder[i] = i + 1;
                LD.PlayerClock[i] = 0.0f;
                LD.TurnDuration[i] = 1.0f;
                Turns[i] = 0.0f;
            }

            if (LD.match == null)
            {
                SetOrder(); //Перемешиваем стандартный порядок игроков
                LD.CurrentPlayer = LD.TurnOrder[0];
            }
            else
            {
                LD.CurrentPlayer = LD.match.Participants.IndexOf(LD.match.Self) + 1;
            }

            //Перемещаем камеру на игрока, который будет управлять первым
            Camera_Jump(LD.TurnOrder[0]);
            if (LD.GameModeCoop)
            {
                for (int i = 0; i < LD.NumberOfPlayers; i++)
                {
                    GameObject.Find("Player_" + LD.TurnOrder[i]).GetComponentsInChildren<SpriteRenderer>()[0].enabled = true;
                    GameObject.Find("Player_" + LD.TurnOrder[i]).GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;
                    FloorTiles[((LD.PlayerPosition[LD.TurnOrder[i] - 1, 0] - 1) / 2),
                        ((LD.PlayerPosition[LD.TurnOrder[i] - 1, 1] - 1) / 2)].GetComponent<SpriteRenderer>().enabled = true;
                }
            }
            else
            {
                //Включаем его рендерер и добавляем начальный тайл пола каждого игрока
                GameObject.Find("Player_" + LD.TurnOrder[0]).GetComponentsInChildren<SpriteRenderer>()[0].enabled = true;
                GameObject.Find("Player_" + LD.TurnOrder[0]).GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;
                FloorTiles[((LD.PlayerPosition[LD.TurnOrder[0] - 1, 0] - 1) / 2),
                        ((LD.PlayerPosition[LD.TurnOrder[0] - 1, 1] - 1) / 2)].GetComponent<SpriteRenderer>().enabled = true;
            }

            for (int i = 0; i < LD.NumberOfPlayers; i++)
            {
                SeenObjects[i].Add(new ObjectsData(ObjectType.FloorTile,
                    ((LD.PlayerPosition[i, 0] - 1) / 2), (LD.PlayerPosition[i, 1] - 1) / 2));
            }

            GameObject.Find("Player_" + LD.TurnOrder[0]).GetComponentsInChildren<SpriteRenderer>()[0].sortingOrder = 2;
        }
        else
        {
            Camera_Jump(LD.CurrentPlayer);
            if (LD.GameModeCoop)
            {
                for (int i = 0; i < LD.NumberOfPlayers; i++)
                {
                    SeenListRenderChange(LD.TurnOrder[i], true);
                    GameObject.Find("Player_" + LD.TurnOrder[i]).GetComponentsInChildren<SpriteRenderer>()[0].enabled = true;
                    GameObject.Find("Player_" + LD.TurnOrder[i]).GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;
                }
            }
            else
            {
                //Включить все объекты текущего игрока
                SeenListRenderChange(LD.CurrentPlayer, true);
                GameObject.Find("Player_" + LD.CurrentPlayer).GetComponentsInChildren<SpriteRenderer>()[0].enabled = true;
                GameObject.Find("Player_" + LD.CurrentPlayer).GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;
                for (int i = 0; i < LD.NumberOfPlayers; i++)
                {
                    if (SeenObjects[LD.CurrentPlayer - 1].Contains(new ObjectsData(ObjectType.FloorTile,
                        ((LD.PlayerPosition[i, 0] - 1) / 2), ((LD.PlayerPosition[i, 1] - 1) / 2))) ||
                        (i + 1) == LD.CurrentPlayer)
                    {
                        GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[0].enabled = true;
                        GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;
                    }
                }
            }

            GameObject.Find("Player_" + LD.CurrentPlayer).GetComponentsInChildren<SpriteRenderer>()[0].sortingOrder = 2;
            Inventory_Interaction.Upd_Inv();
        }

        UpdateTurnClock();
        StartCoroutine("Control_Coroutine"); //Запуск корутины отслеживания нажатий
    }

    //Заполнить и перемешать дефолтный порядок игроков
    private void SetOrder()
    {
        for (int i = 0; i < LD.NumberOfPlayers; i++)
            for (int j = 0; j < LD.NumberOfPlayers; j++)
                if (LD.Seeder.Next(1, 3) == 2 && i != j)
                {
                    int Buf = LD.TurnOrder[i];
                    LD.TurnOrder[i] = LD.TurnOrder[j];
                    LD.TurnOrder[j] = Buf;
                }
    }

    //Перемещение камеры к PlayerNumber игроку
    private void Camera_Jump(int PlayerNumber)
    {
        GameObject.Find("PlayerCamera").transform.position
            = new Vector3(GameObject.Find("Player_" + PlayerNumber).transform.position.x,
            GameObject.Find("Player_" + PlayerNumber).transform.position.y, -4);
    }

    //Корутина ожидания
    IEnumerator Wait(float WaitTime)
    {
        yield return new WaitForSeconds(WaitTime);
    }

    IEnumerator Control_Coroutine()
    {
        while (true)
        {

            //Отдаление или приближение камеры при скролле
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                {
                    Swipe = SwipeDirection.In;
                    CoroutineToStart = "Zoom_In";
                }

                if (Input.GetAxis("Mouse ScrollWheel") < 0)
                {
                    Swipe = SwipeDirection.Out;
                    CoroutineToStart = "Zoom_Out";
                }
            }

            //Определение выбранного направления
            //Контроллер вверх
            if (Input.GetKeyDown("w"))
                Swipe = SwipeDirection.Up;
            //Контроллер вправо
            if (Input.GetKeyDown("d"))
                Swipe = SwipeDirection.Right;
            //Контроллер вниз
            if (Input.GetKeyDown("s"))
                Swipe = SwipeDirection.Down;
            //Контроллер влево
            if (Input.GetKeyDown("a"))
                Swipe = SwipeDirection.Left;

            //Управление свайпами
            //Проверяем, одно ли прикосновение
            if (Input.touchCount == 1)
            {
                Touch TempTouch = Input.GetTouch(0);
                //Если только началось, то сохраним позицию
                if (TempTouch.phase == TouchPhase.Began)
                    InitPoint1 = TempTouch.position;
                //Если закончилось, то сохраним позицию
                if (TempTouch.phase == TouchPhase.Ended)
                {
                    Vector2 Dir = TempTouch.position - InitPoint1;
                    if (Dir.magnitude > DragDistance)
                        if (Math.Abs(Dir.x) < Math.Abs(Dir.y))
                            if (Dir.y > 0)
                                Swipe = SwipeDirection.Up;
                            else
                                Swipe = SwipeDirection.Down;
                        else
                            if (Dir.x > 0)
                            Swipe = SwipeDirection.Right;
                        else
                            Swipe = SwipeDirection.Left;
                }
            }

            if (Input.touchCount == 2)
            {
                Touch TempTouch1 = Input.GetTouch(0);
                Touch TempTouch2 = Input.GetTouch(1);
                if (TempTouch1.phase == TouchPhase.Began)
                    InitPoint1 = TempTouch1.position;
                if (TempTouch1.phase == TouchPhase.Began)
                    InitPoint2 = TempTouch2.position;
                if (TempTouch1.phase == TouchPhase.Moved ||
                    TempTouch2.phase == TouchPhase.Moved)
                {
                    float Distance = Vector2.Distance(InitPoint1, InitPoint2);
                    float NewDistance = Vector2.Distance(TempTouch1.position, TempTouch2.position);
                    if (NewDistance - Distance >= 18.0f)
                    {
                        Swipe = SwipeDirection.In;
                        CoroutineToStart = "Zoom_In";
                        InitPoint1 = TempTouch1.position;
                        InitPoint2 = TempTouch2.position;
                    }
                    if (NewDistance - Distance <= -18.0f)
                    {
                        Swipe = SwipeDirection.Out;
                        CoroutineToStart = "Zoom_Out";
                        InitPoint1 = TempTouch1.position;
                        InitPoint2 = TempTouch2.position;
                    }
                }
            }

            if ((Swipe != SwipeDirection.None && !IsActionActive && CanPlay) || CoroutineToStart == "Turn_Pass")
            {
                yield return StartCoroutine(CoroutineToStart);
                //Обнуляем направление свайпа
                Swipe = SwipeDirection.None;
                if (CoroutineToStart != "Turn_Pass")
                    CoroutineToStart = "Move_Player";
            }
            yield return null;
        }
    }

    IEnumerator Zoom_In()
    {
        if (ZoomOut > 2)
        {
            if (ZoomOut <= 10)
                ZoomOut--;
            else
                ZoomOut -= 2;
            CameraComponent.orthographicSize = ZoomOut;
        }
        Swipe = SwipeDirection.None;
        yield break;
    }

    IEnumerator Zoom_Out()
    {
        if (ZoomOut < 50)
        {
            if (ZoomOut >= 10)
                ZoomOut += 2;
            else
                ZoomOut++;
            CameraComponent.orthographicSize = ZoomOut;
        }
        Swipe = SwipeDirection.None;
        yield break;
    }

    public void Inc_Step()
    {
        Tutorial.Step++;
    }

    IEnumerator Move_Player()
    {
        IsActionActive = true;
        bool Moved = false;
        bool TurnMade = false;
        Quaternion ZeroQuaternion = new Quaternion(0, 0, 0, 0);

        //Исполняем действие перемещения
        if (Swipe != SwipeDirection.None)
        {
            //Получаем компонент "Трансформ" игрока
            Transform TempTransf = GameObject.Find("Player_" + LD.CurrentPlayer).GetComponent<Transform>();
            Quaternion NewRot = TempTransf.rotation;
            Vector3 NewPos = TempTransf.position;

            int OffsetX;
            if (!LD.GameModeOneLabyrinth)
                if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 2))
                {
                    OffsetX = 0;
                }
                else
                {
                    if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 3) * 2 - 1)
                    {
                        OffsetX = 2 * LD.n + 3;
                    }
                    else
                    {
                        if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 3) * 3 - 1)
                        {
                            OffsetX = (2 * LD.n + 3) * 2;
                        }
                        else
                        {
                            OffsetX = (2 * LD.n + 3) * 3;
                        }
                    }
                }
            else
                OffsetX = 0;

            //Уничтожаем паутину, если стояли на ней
            if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 10)
            {
                LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1]] = 0;
                Destroy(GameObject.Find("Web_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
                ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)));
                ObjectsData OD = new ObjectsData(ObjectType.Web,
                            Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2)), Convert.ToInt32((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));
                for (int i = 0; i < LD.NumberOfPlayers; i++)
                {
                    if (SeenObjects[i].Contains(OD))
                        SeenObjects[i].Remove(OD);
                    if (HiddenObjects[i].Contains(OD))
                        HiddenObjects[i].Remove(OD);
                }
            }

            //Если cтоим за пределеами лабиринта, смотрим есть ли в руках сокровище
            if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < OffsetX) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] > 2 * LD.n + OffsetX) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] < 0) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] > 2 * LD.m))
            {
                //Проверяем инвентарь на наличие сокровищ
                for (int i = 0; i < LD.MaxNumberOfItems; i++)
                {
                    //Настоящее сокровище
                    if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.Treasure)
                        SceneManager.LoadSceneAsync("Victory");
                    //Ложное сокровище
                    if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.FalseTreasure)
                    {
                        LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] = LD.InventoryObjects.Nothing;
                        //Убираем спрайт, если открыт инвентарь
                        Inventory_Interaction.Upd_Inv();
                    }
                }
            }

            //Контроллер вверх
            if (Swipe == SwipeDirection.Up)
            {
                if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] < LD.m * 2) &&
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] > OffsetX) &&
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < 2 * LD.n + OffsetX))
                {
                    if (LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1] == 0)
                    {
                        yield return StartCoroutine(Wait(0.05f));

                        NewPos = new Vector3(TempTransf.position.x, TempTransf.position.y + 1, 0);
                        NewRot = ZeroQuaternion;
                        LD.PlayerPosition[LD.CurrentPlayer - 1, 1] += 2;
                        Moved = true;
                        TurnMade = true;
                    }

                    else
                    {
                        SpriteRenderer Rend =
                            Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0]),
                            (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1)].GetComponent<SpriteRenderer>();
                        if (!Rend.enabled)
                        {
                            Rend.enabled = true;
                            SeenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.Wall, LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1));
                            TurnMade = true;
                        }
                    }
                }
            }

            //Контролллер вправо
            if (Swipe == SwipeDirection.Right)
            {
                if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] < LD.m * 2) &&
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] > 0) &&
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < 2 * LD.n + OffsetX))
                {
                    if (LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1, LD.PlayerPosition[LD.CurrentPlayer - 1, 1]] == 0)
                    {
                        yield return StartCoroutine(Wait(0.05f));

                        NewPos = new Vector3(TempTransf.position.x + 1, TempTransf.position.y, 0);
                        NewRot = ZeroQuaternion;
                        LD.PlayerPosition[LD.CurrentPlayer - 1, 0] += 2;
                        Moved = true;
                        TurnMade = true;
                    }

                    else
                    {
                        SpriteRenderer Rend =
                            Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1),
                            (LD.PlayerPosition[LD.CurrentPlayer - 1, 1])].GetComponent<SpriteRenderer>();
                        if (!Rend.enabled)
                        {
                            Rend.enabled = true;
                            SeenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.Wall, LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1, LD.PlayerPosition[LD.CurrentPlayer - 1, 1]));
                            TurnMade = true;
                        }
                    }
                }
            }

            //Контроллер вниз
            if (Swipe == SwipeDirection.Down)
            {
                if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] > 0) &&
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] > OffsetX) &&
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < 2 * LD.n + OffsetX))
                {
                    if (LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1] == 0)
                    {
                        yield return StartCoroutine(Wait(0.05f));

                        NewPos = new Vector3(TempTransf.position.x, TempTransf.position.y - 1, 0);
                        NewRot = ZeroQuaternion;
                        LD.PlayerPosition[LD.CurrentPlayer - 1, 1] -= 2;
                        Moved = true;
                        TurnMade = true;
                    }

                    else
                    {
                        SpriteRenderer Rend =
                            Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0]),
                            (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1)].GetComponent<SpriteRenderer>();
                        if (!Rend.enabled)
                        {
                            Rend.enabled = true;
                            SeenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.Wall, LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1));
                            TurnMade = true;
                        }
                    }
                }
            }

            //Контроллер влево
            if (Swipe == SwipeDirection.Left)
            {
                if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] < LD.m * 2) &&
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] > 0) &&
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] > OffsetX))
                {
                    if (LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1, LD.PlayerPosition[LD.CurrentPlayer - 1, 1]] == 0)
                    {
                        yield return StartCoroutine(Wait(0.05f));

                        NewPos = new Vector3(TempTransf.position.x - 1, TempTransf.position.y, 0);
                        NewRot = ZeroQuaternion;
                        LD.PlayerPosition[LD.CurrentPlayer - 1, 0] -= 2;
                        Moved = true;
                        TurnMade = true;
                    }

                    else
                    {
                        SpriteRenderer Rend =
                            Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1),
                            (LD.PlayerPosition[LD.CurrentPlayer - 1, 1])].GetComponent<SpriteRenderer>();
                        if (!Rend.enabled)
                        {
                            Rend.enabled = true;
                            SeenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.Wall, LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1, LD.PlayerPosition[LD.CurrentPlayer - 1, 1]));
                            TurnMade = true;
                        }
                    }
                }
            }

            //Если оказались за пределеами лабиринта
            if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < OffsetX) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] > 2 * LD.n + OffsetX) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] < 0) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] > 2 * LD.m))
            {
                //Отображаем игрока, если встретили
                if (!LD.GameModeCoop)
                    if (LD.NumberOfPlayers > 1)
                        for (int i = 0; i < LD.NumberOfPlayers; i++)
                        {
                            if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] == LD.PlayerPosition[i, 0] &&
                                LD.PlayerPosition[LD.CurrentPlayer - 1, 1] == LD.PlayerPosition[i, 1] &&
                                i != LD.CurrentPlayer - 1)
                            {
                                GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[0].enabled = true;
                                GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;
                                if (LD.UnderThePlayer[i] == 0)
                                    yield return StartCoroutine(Wait(0.3f));
                            }
                        }
            }

            //Если не за пределами лабиринта, то проверяем, что под игроком
            if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] < LD.m * 2) &&
                 (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] > 0) &&
                 (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] > OffsetX) &&
                 (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < 2 * LD.n + OffsetX) &&
                 Moved)
            {
                LD.UnderThePlayer[LD.CurrentPlayer - 1] = LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1]];

                //Находим значение рендерера тайла пола, на который наступили
                SpriteRenderer Rend = FloorTiles[((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2),
                    ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)].GetComponent<SpriteRenderer>();
                
                if (!Rend.enabled)
                {
                    //Включаем рендерер
                    Rend.enabled = true;
                }

                ObjectsData OD = new ObjectsData(ObjectType.FloorTile,
                    ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2);
                if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                    //Добавляем в увиденные объекты
                    SeenObjects[LD.CurrentPlayer - 1].Add(OD);

                //Отображаем игрока, если встретили
                if (!LD.GameModeCoop)
                    if (LD.NumberOfPlayers > 1)
                        for (int i = 0; i < LD.NumberOfPlayers; i++)
                        {
                            if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] == LD.PlayerPosition[i, 0] &&
                                LD.PlayerPosition[LD.CurrentPlayer - 1, 1] == LD.PlayerPosition[i, 1] &&
                                i != LD.CurrentPlayer - 1)
                            {
                                GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[0].enabled = true;
                                GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;
                                if (LD.UnderThePlayer[i] == 0)
                                    yield return StartCoroutine(Wait(0.3f));
                            }
                        }


                //Если наступили на выход из ямы
                if ((LD.UnderThePlayer[LD.CurrentPlayer - 1] > LD.pt) && (LD.UnderThePlayer[LD.CurrentPlayer - 1] <= 2 * LD.pt))
                {
                    Rend = GameObject.Find("PitExit_" + (OffsetX / (2 * LD.n + 3)) + "_"
                        + (LD.UnderThePlayer[LD.CurrentPlayer - 1] - LD.pt)).GetComponent<SpriteRenderer>();
                    if (!Rend.enabled)
                    {
                        //Показываем, на что наступит игрок
                        Rend.enabled = true;
                        GameObject.Find("PitExit_" + (OffsetX / (2 * LD.n + 3)) + "_"
                            + (LD.UnderThePlayer[LD.CurrentPlayer - 1] - LD.pt)).GetComponentInChildren<MeshRenderer>().enabled = true;
                        yield return StartCoroutine(Wait(0.3f));
                        Chat.Report("PitExitFound", LD.UnderThePlayer[LD.CurrentPlayer - 1] - LD.pt);
                    }
                    OD = new ObjectsData(ObjectType.PitExit, (OffsetX / (2 * LD.n + 3)), (LD.UnderThePlayer[LD.CurrentPlayer - 1] - LD.pt));
                    if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                        //Добавляем в увиденные объекты
                        SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                }

                //Если наступили на яму
                if ((LD.UnderThePlayer[LD.CurrentPlayer - 1] > 0) && (LD.UnderThePlayer[LD.CurrentPlayer - 1] <= LD.pt))
                {
                    //Прорисуем яму
                    GameObject TempPit = GameObject.Find("Pit_" + (OffsetX / (2 * LD.n + 3))
                        + "_" + LD.UnderThePlayer[LD.CurrentPlayer - 1]);
                    if (!TempPit.GetComponent<SpriteRenderer>().enabled)
                    {
                        TempPit.GetComponent<SpriteRenderer>().enabled = true;
                        TempPit.GetComponentInChildren<MeshRenderer>().enabled = true;
                        //Показываем, на что наступит игрок
                        yield return StartCoroutine(Wait(0.3f));
                    }

                    OD = new ObjectsData(ObjectType.Pit, (OffsetX / (2 * LD.n + 3)), LD.UnderThePlayer[LD.CurrentPlayer - 1]);
                    if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                        //Добавляем в увиденные объекты
                        SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                    Chat.Report("FallingIntoPit", LD.UnderThePlayer[LD.CurrentPlayer - 1]);

                    //Переместим игрока и камеру на яму
                    yield return StartCoroutine(MovePlayerAndCamera(NewPos, false));
                    //Найдём, куда перебросить игрока
                    int NewX = 1, NewY = 1;
                    for (int j = 1; j < 2 * LD.m; j += 2)
                        for (int i = 1 + OffsetX; i < 2 * LD.n + OffsetX; i += 2)
                        {
                            if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.Arr[i, j] - LD.pt)
                            {
                                NewX = i;
                                NewY = j;
                            }
                        }
                    NewPos = new Vector3((NewX - 1) / 2.0f, (NewY - 1) / 2.0f, 0);
                    //Сохраним новые координаты
                    LD.PlayerPosition[LD.CurrentPlayer - 1, 0] = NewX;
                    LD.PlayerPosition[LD.CurrentPlayer - 1, 1] = NewY;
                    LD.UnderThePlayer[LD.CurrentPlayer - 1] = LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1]];
                    
                    SoundControlling.PlaySoundEffect("Pit");

                    //Ждём, перед перебросом
                    yield return StartCoroutine(Wait(0.4f));

                    //Прячем других игроков
                    if (!LD.GameModeCoop)
                        for (int i = 0; i < LD.NumberOfPlayers; i++)
                            if (LD.TurnOrder[i] != LD.CurrentPlayer)
                            {
                                GameObject.Find("Player_" + LD.TurnOrder[i]).GetComponentsInChildren<SpriteRenderer>()[0].enabled = false;
                                GameObject.Find("Player_" + LD.TurnOrder[i]).GetComponentsInChildren<SpriteRenderer>()[1].enabled = false;
                            }
                    //Прячем уже посещённые места
                    foreach (ObjectsData Item in SeenObjects[LD.CurrentPlayer - 1])
                    {
                        RenderChange(Item, false);
                    }
                    HiddenObjects[LD.CurrentPlayer - 1].AddRange(SeenObjects[LD.CurrentPlayer - 1]);
                    HiddenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.Interruption, 0, 0));
                    SeenObjects[LD.CurrentPlayer - 1].Clear();
                    if (LD.GameModeCoop)
                        for (int i = 0; i < LD.NumberOfPlayers; i++)
                            SeenListRenderChange(LD.TurnOrder[i], true);

                    //Перебрасываем
                    yield return StartCoroutine(MovePlayerAndCamera(NewPos, true));

                    //Прорисуем тайл пола, на который переброшены и добавим в увиденные обхекты
                    FloorTiles[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2,
                        ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)].GetComponent<SpriteRenderer>().enabled = true;
                    
                    SeenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.FloorTile,
                        ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));

                    //Прорисуем выход из ямы и добавим в увиденные обхекты
                    GameObject.Find("PitExit_" + (OffsetX / (2 * LD.n + 3)) + "_" +
                        (LD.UnderThePlayer[LD.CurrentPlayer - 1] - LD.pt)).GetComponentInChildren<SpriteRenderer>().enabled = true;
                    GameObject.Find("PitExit_" + (OffsetX / (2 * LD.n + 3)) + "_" +
                        (LD.UnderThePlayer[LD.CurrentPlayer - 1] - LD.pt)).GetComponentInChildren<MeshRenderer>().enabled = true;
                    SeenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.PitExit, (OffsetX / (2 * LD.n + 3)), (LD.UnderThePlayer[LD.CurrentPlayer - 1] - LD.pt)));
                }

                //Если наступили на динамит
                if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 2)
                {
                    Rend = GameObject.Find("Dynamite_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
                        ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)).GetComponent<SpriteRenderer>();
                    if (!Rend.enabled)
                    {
                        Rend.enabled = true;
                        //Показываем, на что наступит игрок
                        yield return StartCoroutine(Wait(0.3f));
                    }

                    Pick_Up();

                }


                //Если наступили на сокровище
                if ((LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 3) || (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 4))
                {
                    //Проверяем, есть ли в инвентаре сокровище (нельзя нести больше одного)
                    bool HaveTreasure = false;
                    for (int i = 0; i < LD.MaxNumberOfItems; i++)
                        if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.Treasure ||
                            LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.FalseTreasure)
                            HaveTreasure = true;

                    Rend = GameObject.Find("Treasure_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
                        ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)).GetComponent<SpriteRenderer>();
                    if (!Rend.enabled)
                    {
                        Rend.enabled = true;
                        //Показываем, на что наступит игрок
                        yield return StartCoroutine(Wait(0.3f));
                    }

                    OD = new ObjectsData(ObjectType.Treasure,
                            Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2)), Convert.ToInt32((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));

                    if (HaveTreasure || Inventory_Interaction.IsInventoryFull())
                        if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                            SeenObjects[LD.CurrentPlayer - 1].Add(OD);

                    Pick_Up();

                }

                //Если наступили на ловушку
                if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 5 || LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 7)
                {
                    Rend = GameObject.Find("Spikes_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
                        ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)).GetComponent<SpriteRenderer>();
                    if (!Rend.enabled)
                    {
                        //Показываем, на что наступит игрок
                        Rend.enabled = true;
                        yield return StartCoroutine(Wait(0.3f));
                    }
                    Sprite Picture = Resources.Load<Sprite>("Sprites/SpikesTriggered");
                    if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 5)
                    {
                        //Замедлим игрока
                        LD.TurnDuration[LD.CurrentPlayer - 1] *= 2.0f;
                        Chat.Report("SlowDown");
                        //Создадим взрыв
                        Vector3 DetPos = new Vector3(-0.5f + LD.PlayerPosition[LD.CurrentPlayer - 1, 0] * 0.5f, -0.5f + LD.PlayerPosition[LD.CurrentPlayer - 1, 1] * 0.5f, 0);
                        Quaternion DetRot = new Quaternion(0, 0, 0, 0);
                        CreateEffect(DetPos, DetRot, "MineDetonation");
                    }
                    LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1]] = LD.pt * 2 + 7;
                    Rend.sprite = Picture;
                    Resources.UnloadUnusedAssets();
                    OD = new ObjectsData(ObjectType.Spikes,
                                Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2)), Convert.ToInt32((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));
                    if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                        //Добавляем в увиденные объекты
                        SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                }

                //Если наступили на ускоритель
                if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 6 || LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 8)
                {
                    Rend = GameObject.Find("SpeedUp_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
                        ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)).GetComponent<SpriteRenderer>();
                    if (!Rend.enabled)
                    {
                        //Показываем, на что наступит игрок
                        Rend.enabled = true;
                        yield return StartCoroutine(Wait(0.3f));
                    }
                    Sprite Picture = Resources.Load<Sprite>("Sprites/SpeedUpTriggered");
                    if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 6)
                    {
                        SoundControlling.PlaySoundEffect("SpeedUp");

                        LD.TurnDuration[LD.CurrentPlayer - 1] /= 2.0f;
                        Chat.Report("SpeedUp");
                    }
                    LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1]] = LD.pt * 2 + 8;
                    Rend.sprite = Picture;
                    Resources.UnloadUnusedAssets();

                    OD = new ObjectsData(ObjectType.SpeedUp,
                                Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2)), Convert.ToInt32((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));
                    if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                        //Добавляем в увиденные объекты
                        SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                }

                //Если наступили на фонарь
                if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 9)
                {
                    Rend = GameObject.Find("Matches_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
                        ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)).GetComponent<SpriteRenderer>();
                    if (!Rend.enabled)
                    {
                        Rend.enabled = true;
                        //Показываем, на что наступит игрок
                        yield return StartCoroutine(Wait(0.3f));
                    }

                    Pick_Up();

                }

                //Если наступили на паутину
                if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 10)
                {
                    Rend = GameObject.Find("Web_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
                        ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)).GetComponent<SpriteRenderer>();
                    if (!Rend.enabled)
                    {
                        Rend.enabled = true;
                        //Показываем, на что наступит игрок
                        yield return StartCoroutine(Wait(0.3f));
                    }
                    float Hold = LD.Seeder.Next(1, 4) * LD.TurnDuration[LD.CurrentPlayer - 1];
                    LD.PlayerClock[LD.CurrentPlayer - 1] += Hold;
                    if (Turns[LD.CurrentPlayer - 1] <= 10000.0f)
                        Turns[LD.CurrentPlayer - 1] += Hold;
                    Chat.Report("StuckInWeb", Hold);

                    SoundControlling.PlaySoundEffect("Octopus");

                    OD = new ObjectsData(ObjectType.Web,
                                Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2)), Convert.ToInt32((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));
                    if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                        //Добавляем в увиденные объекты
                        SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                }

                //Если наступили на пещеру
                if (!LD.GameModeOneLabyrinth)
                {
                    if (LD.UnderThePlayer[LD.CurrentPlayer - 1] >= 2 * LD.pt + 11)
                    {
                        EnterCaveButton.SetActive(true);
                        GameObject TempObj = GameObject.Find("Cave_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
                        ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));
                        if (!TempObj.GetComponent<SpriteRenderer>().enabled)
                        {
                            TempObj.GetComponent<SpriteRenderer>().enabled = true;
                            TempObj.GetComponentInChildren<MeshRenderer>().enabled = true;
                            //Показываем, на что наступит игрок
                            yield return StartCoroutine(Wait(0.3f));
                        }

                        OD = new ObjectsData(ObjectType.Cave, Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2)),
                            Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)));
                        if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                        {
                            //Добавляем в увиденные объекты
                            SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                            Chat.Report("CaveFound", LD.UnderThePlayer[LD.CurrentPlayer - 1] - (LD.pt * 2 + 11));
                        }
                    }
                    else
                    {
                        EnterCaveButton.SetActive(false);
                    }
                }
            }

            //Переместим игрока и камеру, если ещё нужно
            if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] != NewPos.x || LD.PlayerPosition[LD.CurrentPlayer - 1, 1] != NewPos.y)
            {
                yield return StartCoroutine(MovePlayerAndCamera(NewPos, false));

                //Если оказались за пределеами лабиринта, смотрим есть ли в руках сокровище
                if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < OffsetX) ||
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] > 2 * LD.n + OffsetX) ||
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] < 0) ||
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] > 2 * LD.m))
                {
                    //Проверяем инвентарь на наличие сокровищ
                    for (int i = 0; i < LD.MaxNumberOfItems; i++)
                    {
                        //Настоящее сокровище
                        if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.Treasure)
                            SceneManager.LoadSceneAsync("Victory");
                        //Ложное сокровище
                        if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.FalseTreasure)
                        {
                            LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] = LD.InventoryObjects.Nothing;
                            //Убираем спрайт, если открыт инвентарь
                            Inventory_Interaction.Upd_Inv();
                            Chat.Report("FalseTreasureBrought");
                            SoundControlling.PlaySoundEffect("FalseTreasure");
                        }
                    }
                }
            }

            if (TurnMade)
                IncTurnClock();
            if (TurnMade && LD.NumberOfPlayers != 1)
                yield return StartCoroutine(Turn_Pass());
            else
            {
                IsActionActive = false;
            }
        }

        yield break;
    }

    //Запрос, посещалось ли место
    public static bool AskIfVisited()
    {
        IncTurnClock();
        bool Visited = false, Restart;
        //Включение рендера посещённых и удвление из списка спрятанных
        do
        {
            Restart = false;
            foreach (ObjectsData Item in SeenObjects[LD.CurrentPlayer - 1])
            {
                if (HiddenObjects[LD.CurrentPlayer - 1].Contains(Item))
                {
                    MoveFromHiddenToSeen(Item);
                    Visited = true;
                    Restart = true;
                    break;
                }
            }
        } while (Restart);
        if (Visited)
            for (int i = 0; i < LD.NumberOfPlayers; i++)
            {
                if (SeenObjects[LD.CurrentPlayer - 1].Contains(new ObjectsData(ObjectType.FloorTile,
                    ((LD.PlayerPosition[i, 0] - 1) / 2), ((LD.PlayerPosition[i, 1] - 1) / 2))) ||
                    (i + 1) == LD.CurrentPlayer)
                {
                    GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[0].enabled = true;
                    GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;
                }
            }
        return (Visited);
    }

    //Извлечение группы объектов из Hidden в Seen, в которой находится OD
    private static void MoveFromHiddenToSeen(ObjectsData OD)
    {
        //Пока не извдечём сам объект
        while (HiddenObjects[LD.CurrentPlayer - 1].Contains(OD))
        {
            //Если он не первый
            if (HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) > 0)
            {
                //Если он не последний
                if (HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) < HiddenObjects[LD.CurrentPlayer - 1].Count - 1)
                {
                    //Если перед ним - прерывание
                    if (HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) - 1].Obj == ObjectType.Interruption)
                    {
                        //Если после него - прерывание
                        if (HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) + 1].Obj == ObjectType.Interruption)
                        {
                            HiddenObjects[LD.CurrentPlayer - 1].RemoveAt(HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) - 1);
                            SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                            HiddenObjects[LD.CurrentPlayer - 1].Remove(OD);
                        }
                        //Если после него - не прерывание
                        else
                        {
                            RenderChange(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) + 1], true);
                            SeenObjects[LD.CurrentPlayer - 1].Add(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) + 1]);
                            HiddenObjects[LD.CurrentPlayer - 1].Remove(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) + 1]);
                        }
                    }
                    //Если перед ним - не прерывание
                    else
                    {
                        RenderChange(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) - 1], true);
                        SeenObjects[LD.CurrentPlayer - 1].Add(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) - 1]);
                        HiddenObjects[LD.CurrentPlayer - 1].Remove(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) - 1]);
                    }
                }
                //Если он последний
                else
                {
                    //Если перед ним - прерывание
                    if (HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) - 1].Obj == ObjectType.Interruption)
                    {
                        HiddenObjects[LD.CurrentPlayer - 1].RemoveAt(HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) - 1);
                        SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                        HiddenObjects[LD.CurrentPlayer - 1].Remove(OD);
                    }
                    //Если перед ним - не прерывание
                    else
                    {
                        RenderChange(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) - 1], true);
                        SeenObjects[LD.CurrentPlayer - 1].Add(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) - 1]);
                        HiddenObjects[LD.CurrentPlayer - 1].Remove(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) - 1]);
                    }
                }
            }
            //Если он первый
            else
            {
                //Если после него - прерывание
                if (HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) + 1].Obj == ObjectType.Interruption)
                {
                    HiddenObjects[LD.CurrentPlayer - 1].RemoveAt(HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) + 1);
                    SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                    HiddenObjects[LD.CurrentPlayer - 1].Remove(OD);
                }
                //Если после него - не прерывание
                else
                {
                    RenderChange(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) + 1], true);
                    SeenObjects[LD.CurrentPlayer - 1].Add(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) + 1]);
                    HiddenObjects[LD.CurrentPlayer - 1].Remove(HiddenObjects[LD.CurrentPlayer - 1][HiddenObjects[LD.CurrentPlayer - 1].IndexOf(OD) + 1]);
                }
            }
        }
        return;
    }

    //Включение/выключение рендерера объекта типа ObjectsData
    private static void RenderChange(ObjectsData OD, bool RendEnabled)
    {
        if (OD.Obj == ObjectType.FloorTile)
        {
            FloorTiles[OD.x, OD.y].GetComponent<SpriteRenderer>().enabled = RendEnabled;
        }
        else
        {
            if (OD.Obj == ObjectType.Wall)
            {
                Walls[OD.x,OD.y].GetComponent<SpriteRenderer>().enabled = RendEnabled;
            }
            else
            {
                GameObject.Find(OD.Obj + "_" + OD.x + "_" + OD.y).GetComponent<SpriteRenderer>().enabled = RendEnabled;
                if (OD.Obj == ObjectType.Pit || OD.Obj == ObjectType.PitExit || OD.Obj == ObjectType.Cave)
                {
                    GameObject.Find(OD.Obj + "_" + OD.x + "_" + OD.y).GetComponentInChildren<MeshRenderer>().enabled = RendEnabled;
                }
            }
        }
    }

    private static void SeenListRenderChange(int PlayerNumber, bool RendEnabled)
    {
        foreach (ObjectsData Item in SeenObjects[PlayerNumber - 1])
        {
            RenderChange(Item, RendEnabled);
        }
    }

    //Создать значок (галочка или взрыв)
    private void CreateEffect(Vector3 Pos, Quaternion Rot, string Effect)
    {
        GameObject EffectGO = Resources.Load<GameObject>("Prefabs/" + Effect);
        GameObject TempGO = Instantiate(EffectGO, Pos, Rot);
        TempGO.name = Effect;
        Resources.UnloadUnusedAssets();
        return;
    }

    //Перемещаем игрока и камеру
    public static IEnumerator MovePlayerAndCamera(Vector3 Pos, bool Jump)
    {
        Transform PlayerTransform = GameObject.Find("Player_" + LD.CurrentPlayer).GetComponent<Transform>(),
        CameraTransform = GameObject.Find("PlayerCamera").GetComponent<Transform>();
        if (Jump)
        {
            PlayerTransform.position = Pos;
            CameraTransform.position = new Vector3(Pos.x, Pos.y, -4);
            yield break;
        }
        else
        {
            bool PosGreater = (PlayerTransform.position.x < Pos.x || PlayerTransform.position.y < Pos.y);
            while (PlayerTransform.position!=Pos)
            {
                if (PlayerTransform.position.x != Pos.x)
                {
                    if (Mathf.Abs(PlayerTransform.position.x - Pos.x) < 0.08f)
                    {
                        PlayerTransform.position = Pos;
                        CameraTransform.position = new Vector3(Pos.x, Pos.y, -4);
                    }
                    else
                    {
                        if (PosGreater)
                        {
                            PlayerTransform.position = new Vector3(PlayerTransform.position.x + ((Pos.x - PlayerTransform.position.x) / 8f + 0.015f), Pos.y, 0);
                        }
                        else
                        {
                            PlayerTransform.position = new Vector3(PlayerTransform.position.x + ((Pos.x - PlayerTransform.position.x) / 8f - 0.015f), Pos.y, 0);
                        }
                    }
                }
                else
                {
                    if (Mathf.Abs(PlayerTransform.position.y - Pos.y) < 0.08f)
                    {
                        PlayerTransform.position = Pos;
                        CameraTransform.position = new Vector3(Pos.x, Pos.y, -4);
                    }
                    else
                    {
                        if (PosGreater)
                        {
                            PlayerTransform.position = new Vector3(Pos.x, PlayerTransform.position.y + ((Pos.y - PlayerTransform.position.y) / 8f + 0.015f), 0);
                        }
                        else
                        {
                            PlayerTransform.position = new Vector3(Pos.x, PlayerTransform.position.y + ((Pos.y - PlayerTransform.position.y) / 8f - 0.015f), 0);
                        }
                    }
                }
                CameraTransform.position = new Vector3(PlayerTransform.position.x, PlayerTransform.position.y, -4);
                yield return null;
            }
            yield break;
        }
    }

    //Корутина взрыва
    IEnumerator Detonate()
    {
        IsActionActive = true;
        int OffsetX;
        if (!LD.GameModeOneLabyrinth)
            if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 2))
            {
                OffsetX = 0;
            }
            else
            {
                if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 3) * 2 - 1)
                {
                    OffsetX = 2 * LD.n + 3;
                }
                else
                {
                    if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 3) * 3 - 1)
                    {
                        OffsetX = (2 * LD.n + 3) * 2;
                    }
                    else
                    {
                        OffsetX = (2 * LD.n + 3) * 3;
                    }
                }
            }
        else
            OffsetX = 0;

        if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < OffsetX) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] > 2 * LD.n + OffsetX) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] < 0) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] > 2 * LD.m))
        {
            Chat.Report("CantDetonate");
            //Изменяем цвет прорисовки клетки инвентаря обратно на стандартный
            GameObject.Find("Inventory_Cell_" + Inventory_Interaction.CellClicked).GetComponent<Image>().color = Color.white;
            //Обнулим указатель выбранной клетки
            Inventory_Interaction.CellClicked = 0;
            IsActionActive = false;
            yield break;
        }
        bool Detonated = false;

        //Контроллер вверх
        if (Swipe == SwipeDirection.Up)
        {
            if (LD.NumberOfPlayers > 1)
                if (!LD.GameModeCoop)
                    for (int i = 0; i < LD.NumberOfPlayers; i++)
                        if (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 2 == LD.PlayerPosition[i, 1] &&
                            LD.PlayerPosition[LD.CurrentPlayer - 1, 0] == LD.PlayerPosition[i, 0])
                        {
                            int Hold = LD.Seeder.Next(3, 9);
                            LD.PlayerClock[i] += LD.TurnDuration[i] * Hold;
                            if (Turns[i] <= 10000.0f)
                                Turns[i] += LD.TurnDuration[i] * Hold;
                            Detonated = true;
                            Chat.Report("StunnedWithDynamite", Hold, i + 1);
                            //Удалим динамит из выбранной клетки
                            LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] = LD.InventoryObjects.Nothing;
                            Check4TreasuresNDrop(i);
                        }
            if (Detonated)
            {
                yield return StartCoroutine(Wait(0.025f));
                Vector3 DetPos = new Vector3(-0.5f + LD.PlayerPosition[LD.CurrentPlayer - 1, 0] * 0.5f, -0.5f + (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1) * 0.5f, 0);
                Quaternion DetRot = new Quaternion(0, 0, 0, 0);
                CreateEffect(DetPos, DetRot, "Detonation");
                yield return StartCoroutine(Wait(0.20f));
            }
            if (LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1] == 1)
            {
                if (Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0]),
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1)].GetComponent<SpriteRenderer>().enabled || Detonated)
                {
                    if (!Detonated)
                    {
                        yield return StartCoroutine(Wait(0.025f));
                        Vector3 DetPos = new Vector3(-0.5f + LD.PlayerPosition[LD.CurrentPlayer - 1, 0] * 0.5f, -0.5f + (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1) * 0.5f, 0);
                        Quaternion DetRot = new Quaternion(0, 0, 0, 0);
                        CreateEffect(DetPos, DetRot, "Detonation");
                        yield return StartCoroutine(Wait(0.20f));
                    }

                    Destroy(Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0]),
                        (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1)]);
                    Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0]), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1)] = null;
                    LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1] = 0;
                    ObjectsData OD = new ObjectsData(ObjectType.Wall, (LD.PlayerPosition[LD.CurrentPlayer - 1, 0]), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] + 1));
                    for (int i = 0; i < LD.NumberOfPlayers; i++)
                    {
                        if (HiddenObjects[i].Contains(OD))
                            HiddenObjects[i].Remove(OD);
                        if (SeenObjects[i].Contains(OD))
                            SeenObjects[i].Remove(OD);
                    }
                    Detonated = true;
                }
            }
        }

        //Контролллер вправо
        if (Swipe == SwipeDirection.Right)
        {
            if (LD.NumberOfPlayers > 1)
                if (!LD.GameModeCoop)
                    for (int i = 0; i < LD.NumberOfPlayers; i++)
                        if (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] == LD.PlayerPosition[i, 1] &&
                            LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 2 == LD.PlayerPosition[i, 0])
                        {
                            int Hold = LD.Seeder.Next(3, 9);
                            LD.PlayerClock[i] += LD.TurnDuration[i] * Hold;
                            if (Turns[i] <= 10000.0f)
                                Turns[i] += LD.TurnDuration[i] * Hold;
                            Detonated = true;
                            Chat.Report("StunnedWithDynamite", Hold, i + 1);
                            //Удалим динамит из выбранной клетки
                            LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] = LD.InventoryObjects.Nothing;
                            Check4TreasuresNDrop(i);
                        }
            if (Detonated)
            {
                yield return StartCoroutine(Wait(0.025f));
                Vector3 DetPos = new Vector3(-0.5f + (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1) * 0.5f, -0.5f + (LD.PlayerPosition[LD.CurrentPlayer - 1, 1]) * 0.5f, 0);
                Quaternion DetRot = new Quaternion(0, 0, 0, 0);
                CreateEffect(DetPos, DetRot, "Detonation");
                yield return StartCoroutine(Wait(0.20f));
            }
            if (LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1, LD.PlayerPosition[LD.CurrentPlayer - 1, 1]] == 1)
            {
                if (Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1),
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 1])].GetComponent<SpriteRenderer>().enabled || Detonated)
                {
                    if (!Detonated)
                    {
                        yield return StartCoroutine(Wait(0.025f));
                        Vector3 DetPos = new Vector3(-0.5f + (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1) * 0.5f, -0.5f + LD.PlayerPosition[LD.CurrentPlayer - 1, 1] * 0.5f, 0);
                        Quaternion DetRot = new Quaternion(0, 0, 0, 0);
                        CreateEffect(DetPos, DetRot, "Detonation");
                        yield return StartCoroutine(Wait(0.20f));
                    }

                    Destroy(Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1),
                        (LD.PlayerPosition[LD.CurrentPlayer - 1, 1])]);
                    Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1])] = null;
                    LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1, LD.PlayerPosition[LD.CurrentPlayer - 1, 1]] = 0;
                    ObjectsData OD = new ObjectsData(ObjectType.Wall, (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] + 1), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1]));
                    for (int i = 0; i < LD.NumberOfPlayers; i++)
                    {
                        if (HiddenObjects[i].Contains(OD))
                            HiddenObjects[i].Remove(OD);
                        if (SeenObjects[i].Contains(OD))
                            SeenObjects[i].Remove(OD);
                    }
                    Detonated = true;
                }
            }
        }

        //Контроллер вниз
        if (Swipe == SwipeDirection.Down)
        {
            if (LD.NumberOfPlayers > 1)
                if (!LD.GameModeCoop)
                    for (int i = 0; i < LD.NumberOfPlayers; i++)
                        if (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 2 == LD.PlayerPosition[i, 1] &&
                            LD.PlayerPosition[LD.CurrentPlayer - 1, 0] == LD.PlayerPosition[i, 0])
                        {
                            int Hold = LD.Seeder.Next(3, 9);
                            LD.PlayerClock[i] += LD.TurnDuration[i] * Hold;
                            if (Turns[i] <= 10000.0f)
                                Turns[i] += LD.TurnDuration[i] * Hold;
                            Detonated = true;
                            Chat.Report("StunnedWithDynamite", Hold, i + 1);
                            //Удалим динамит из выбранной клетки
                            LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] = LD.InventoryObjects.Nothing;
                            Check4TreasuresNDrop(i);
                        }
            if (Detonated)
            {
                yield return StartCoroutine(Wait(0.025f));
                Vector3 DetPos = new Vector3(-0.5f + LD.PlayerPosition[LD.CurrentPlayer - 1, 0] * 0.5f, -0.5f + (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) * 0.5f, 0);
                Quaternion DetRot = new Quaternion(0, 0, 0, 0);
                CreateEffect(DetPos, DetRot, "Detonation");
                yield return StartCoroutine(Wait(0.20f));
            }
            if (LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1] == 1)
            {
                if (Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0]),
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1)].GetComponent<SpriteRenderer>().enabled || Detonated)
                {
                    if (!Detonated)
                    {
                        yield return StartCoroutine(Wait(0.025f));
                        Vector3 DetPos = new Vector3(-0.5f + LD.PlayerPosition[LD.CurrentPlayer - 1, 0] * 0.5f, -0.5f + (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) * 0.5f, 0);
                        Quaternion DetRot = new Quaternion(0, 0, 0, 0);
                        CreateEffect(DetPos, DetRot, "Detonation");
                        yield return StartCoroutine(Wait(0.20f));
                    }

                    Destroy(Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0]),
                        (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1)]);
                    Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0]), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1)] = null;
                    LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1] = 0;
                    ObjectsData OD = new ObjectsData(ObjectType.Wall, (LD.PlayerPosition[LD.CurrentPlayer - 1, 0]), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1));
                    for (int i = 0; i < LD.NumberOfPlayers; i++)
                    {
                        if (HiddenObjects[i].Contains(OD))
                            HiddenObjects[i].Remove(OD);
                        if (SeenObjects[i].Contains(OD))
                            SeenObjects[i].Remove(OD);
                    }
                    Detonated = true;
                }
            }
        }

        //Контроллер влево
        if (Swipe == SwipeDirection.Left)
        {
            if (LD.NumberOfPlayers > 1)
                if (!LD.GameModeCoop)
                    for (int i = 0; i < LD.NumberOfPlayers; i++)
                        if (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] == LD.PlayerPosition[i, 1] &&
                            LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 2 == LD.PlayerPosition[i, 0])
                        {
                            int Hold = LD.Seeder.Next(3, 9);
                            LD.PlayerClock[i] += LD.TurnDuration[i] * Hold;
                            if (Turns[i] <= 10000.0f)
                                Turns[i] += LD.TurnDuration[i] * Hold;
                            Detonated = true;
                            Chat.Report("StunnedWithDynamite", Hold, i + 1);
                            //Удалим динамит из выбранной клетки
                            LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] = LD.InventoryObjects.Nothing;
                            Check4TreasuresNDrop(i);
                        }
            if (Detonated)
            {
                yield return StartCoroutine(Wait(0.025f));
                Vector3 DetPos = new Vector3(-0.5f + (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) * 0.5f, -0.5f + (LD.PlayerPosition[LD.CurrentPlayer - 1, 1]) * 0.5f, 0);
                Quaternion DetRot = new Quaternion(0, 0, 0, 0);
                CreateEffect(DetPos, DetRot, "Detonation");
                yield return StartCoroutine(Wait(0.20f));
            }
            if (LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1, LD.PlayerPosition[LD.CurrentPlayer - 1, 1]] == 1)
            {
                if (Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1),
                    (LD.PlayerPosition[LD.CurrentPlayer - 1, 1])].GetComponent<SpriteRenderer>().enabled || Detonated)
                {
                    if (!Detonated)
                    {
                        yield return StartCoroutine(Wait(0.025f));
                        Vector3 DetPos = new Vector3(-0.5f + (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) * 0.5f, -0.5f + LD.PlayerPosition[LD.CurrentPlayer - 1, 1] * 0.5f, 0);
                        Quaternion DetRot = new Quaternion(0, 0, 0, 0);
                        CreateEffect(DetPos, DetRot, "Detonation");
                        yield return StartCoroutine(Wait(0.20f));
                    }

                    Destroy(Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1),
                        (LD.PlayerPosition[LD.CurrentPlayer - 1, 1])]);
                    Walls[(LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1])] = null;
                    LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1, LD.PlayerPosition[LD.CurrentPlayer - 1, 1]] = 0;
                    ObjectsData OD = new ObjectsData(ObjectType.Wall, (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1]));
                    for (int i = 0; i < LD.NumberOfPlayers; i++)
                    {
                        if (HiddenObjects[i].Contains(OD))
                            HiddenObjects[i].Remove(OD);
                        if (SeenObjects[i].Contains(OD))
                            SeenObjects[i].Remove(OD);
                    }
                    Detonated = true;
                }
            }
        }

        //Если мы что-то взорвали
        if (Detonated)
        {
            if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] != LD.InventoryObjects.FalseTreasure &&
                LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] != LD.InventoryObjects.Treasure)
            {
                //Удалим динамит из выбранной клетки
                LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] = LD.InventoryObjects.Nothing;
                Inventory_Interaction.Upd_Inv();
            }
            IncTurnClock();
        }

        //Изменяем цвет прорисовки клетки инвентаря обратно на стандартный
        Image TempImg = GameObject.Find("Inventory_Cell_" + Inventory_Interaction.CellClicked).GetComponent<Image>();
        TempImg.color = Color.white;

        //Обнулим указатель выбранной клетки
        Inventory_Interaction.CellClicked = 0;

        Swipe = SwipeDirection.None;
        Resources.UnloadUnusedAssets();
        if (Detonated && LD.NumberOfPlayers != 1)
            yield return StartCoroutine(Turn_Pass());
        else
            IsActionActive = false;
        yield break;
    }

    private void Check4TreasuresNDrop(int i)
    {
        bool HaveTreasure = false;
        for (int k = 0; k < LD.MaxNumberOfItems; k++)
            if (LD.InInventory[LD.CurrentPlayer - 1, k] == LD.InventoryObjects.Treasure ||
                LD.InInventory[LD.CurrentPlayer - 1, k] == LD.InventoryObjects.FalseTreasure)
            {
                HaveTreasure = true;
            }
        for (int j = 0; j < LD.MaxNumberOfItems; j++)
        {
            if (LD.InInventory[i, j] == LD.InventoryObjects.Treasure)
            {
                if (!HaveTreasure)
                {
                    LD.InInventory[i, j] = LD.InventoryObjects.Nothing;
                    for (int k = 0; k < LD.MaxNumberOfItems; k++)
                    {
                        if (LD.InInventory[LD.CurrentPlayer - 1, k] == LD.InventoryObjects.Nothing)
                        {
                            LD.InInventory[LD.CurrentPlayer - 1, k] = LD.InventoryObjects.Treasure;
                            Inventory_Interaction.Upd_Inv();
                            Chat.Report("TreasureStolen", i + 1);
                            break;
                        }
                    }
                }
                else
                {
                    Chat.Report("CantSteal", i + 1);
                }
            }
            if (LD.InInventory[i, j] == LD.InventoryObjects.FalseTreasure)
            {
                if (!HaveTreasure)
                {
                    LD.InInventory[i, j] = LD.InventoryObjects.Nothing;
                    for (int k = 0; k < LD.MaxNumberOfItems; k++)
                    {
                        if (LD.InInventory[LD.CurrentPlayer - 1, k] == LD.InventoryObjects.Nothing)
                        {
                            LD.InInventory[LD.CurrentPlayer - 1, k] = LD.InventoryObjects.FalseTreasure;
                            Inventory_Interaction.Upd_Inv();
                            Chat.Report("TreasureStolen", i + 1);
                            break;
                        }
                    }
                }
                else
                {
                    Chat.Report("CantSteal", i + 1);
                }
            }
        }
    }

    //Корутина освещения пути фонарём
    IEnumerator Light_The_Match()
    {
        IsActionActive = true;
        int OffsetX;
        if (!LD.GameModeOneLabyrinth)
            if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 2))
            {
                OffsetX = 0;
            }
            else
            {
                if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 3) * 2 - 1)
                {
                    OffsetX = 2 * LD.n + 3;
                }
                else
                {
                    if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 3) * 3 - 1)
                    {
                        OffsetX = (2 * LD.n + 3) * 2;
                    }
                    else
                    {
                        OffsetX = (2 * LD.n + 3) * 3;
                    }
                }
            }
        else
            OffsetX = 0;
        if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < OffsetX) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] > 2 * LD.n + OffsetX) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] < 0) ||
                (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] > 2 * LD.m))
        {
            Chat.Report("CantLight");
            //Изменяем цвет прорисовки клетки инвентаря обратно на стандартный
            GameObject.Find("Inventory_Cell_" + Inventory_Interaction.CellClicked).GetComponent<Image>().color = Color.white;
            //Обнулим указатель выбранной клетки
            Inventory_Interaction.CellClicked = 0;
            IsActionActive = false;
            yield break;
        }

        CreateEffect(GameObject.Find("Player_" + LD.CurrentPlayer).transform.position,
            new Quaternion(0, 0, 0, 0), "Light");
        yield return StartCoroutine(Wait(0.8f));

        //Присвоение текущей позиции
        int i = LD.PlayerPosition[LD.CurrentPlayer - 1, 0];
        int j = LD.PlayerPosition[LD.CurrentPlayer - 1, 1];
        //Поиск вверх
        while (LD.Arr[i, j + 1] != 1 && j != 2 * LD.m - 1)
        {
            j += 2;
            Activate_Current(i, j, OffsetX);
        }
        if (LD.Arr[i, ++j] == 1)
        {
            SpriteRenderer Rend =
                        Walls[i,j].GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
                SeenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.Wall, i, j));
            }
        }

        //Присвоение текущей позиции
        i = LD.PlayerPosition[LD.CurrentPlayer - 1, 0];
        j = LD.PlayerPosition[LD.CurrentPlayer - 1, 1];
        //Поиск вправо
        while (LD.Arr[i + 1, j] != 1 && i != OffsetX + 2 * LD.n - 1)
        {
            i += 2;
            Activate_Current(i, j, OffsetX);
        }
        if (LD.Arr[++i, j] == 1)
        {
            SpriteRenderer Rend =
                        Walls[i,j].GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
                SeenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.Wall, i, j));
            }
        }

        //Присвоение текущей позиции
        i = LD.PlayerPosition[LD.CurrentPlayer - 1, 0];
        j = LD.PlayerPosition[LD.CurrentPlayer - 1, 1];
        //Поиск вниз
        while (LD.Arr[i, j - 1] != 1 && j != 1)
        {
            j -= 2;
            Activate_Current(i, j, OffsetX);
        }
        if (LD.Arr[i, --j] == 1)
        {
            SpriteRenderer Rend =
                        Walls[i, j].GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
                SeenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.Wall, i, j));
            }
        }

        //Присвоение текущей позиции
        i = LD.PlayerPosition[LD.CurrentPlayer - 1, 0];
        j = LD.PlayerPosition[LD.CurrentPlayer - 1, 1];
        //Поиск влево
        while (LD.Arr[i - 1, j] != 1 && i != OffsetX + 1)
        {
            i -= 2;
            Activate_Current(i, j, OffsetX);
        }
        if (LD.Arr[--i, j] == 1)
        {
            SpriteRenderer Rend =
                        Walls[i, j].GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
                SeenObjects[LD.CurrentPlayer - 1].Add(new ObjectsData(ObjectType.Wall, i, j));
            }
        }
        bool Reported = false;
        for (int k = 0; k < LD.MaxNumberOfItems; k++)
            if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), k] == LD.InventoryObjects.FalseTreasure)
            {
                LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), k] = LD.InventoryObjects.Nothing;
                Inventory_Interaction.Upd_Inv();
                Chat.Report("MatchesLit", 1);
                Reported = true;
            }
        if (!Reported)
            Chat.Report("MatchesLit", 0);

        //Изменяем цвет прорисовки клетки инвентаря обратно на стандартный
        Image TempImg = GameObject.Find("Inventory_Cell_" + Inventory_Interaction.CellClicked).GetComponent<Image>();
        TempImg.color = Color.white;

        //Удалим фонарь из выбранной клетки
        LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] = LD.InventoryObjects.Nothing;
        Inventory_Interaction.Upd_Inv();
        IncTurnClock();

        //Обнулим указатель выбранной клетки
        Inventory_Interaction.CellClicked = 0;
        Resources.UnloadUnusedAssets();
        Swipe = SwipeDirection.None;
        if (LD.NumberOfPlayers != 1)
            yield return StartCoroutine(Turn_Pass());
        else
            IsActionActive = false;
        yield break;
    }

    //Включить рендерер текущего объекта
    private void Activate_Current(int i, int j, int OffsetX)
    {
        //Активируем клетку пола
        //Находим значение рендерера тайла пола
        SpriteRenderer Rend = FloorTiles[((i - 1) / 2),
            ((j - 1) / 2)].GetComponent<SpriteRenderer>();
        
        if (!Rend.enabled)
        {
            //Включаем рендерер
            Rend.enabled = true;
        }
        ObjectsData OD = new ObjectsData(ObjectType.FloorTile,
            ((i - 1) / 2), (j - 1) / 2);
        if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
            //Добавляем в увиденные объекты
            SeenObjects[LD.CurrentPlayer - 1].Add(OD);

        //И то, что на ней
        //Отображаем игрока, если нашли
        if (LD.NumberOfPlayers > 1)
        {
            for (int k = 0; k < LD.NumberOfPlayers; k++)
            {
                if (i == LD.PlayerPosition[k, 0] &&
                    j == LD.PlayerPosition[k, 1])
                {
                    GameObject.Find("Player_" + (k + 1)).GetComponentsInChildren<SpriteRenderer>()[0].enabled = true;
                    GameObject.Find("Player_" + (k + 1)).GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;
                }
            }
        }

        //Если выход из ямы
        if ((LD.Arr[i, j] > LD.pt) && (LD.Arr[i, j] <= 2 * LD.pt))
        {
            Rend = GameObject.Find("PitExit_" + (OffsetX / (2 * LD.n + 3)) + "_"
                + (LD.Arr[i, j] - LD.pt)).GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                //Показываем
                Rend.enabled = true;
                GameObject.Find("PitExit_" + (OffsetX / (2 * LD.n + 3)) + "_"
                    + (LD.Arr[i, j] - LD.pt)).GetComponentInChildren<MeshRenderer>().enabled = true;
            }
            OD = new ObjectsData(ObjectType.PitExit, (OffsetX / (2 * LD.n + 3)), (LD.Arr[i, j] - LD.pt));
            if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                //Добавляем в увиденные объекты
                SeenObjects[LD.CurrentPlayer - 1].Add(OD);
        }

        //Если водоворот
        if ((LD.Arr[i, j] > 0) && (LD.Arr[i, j] <= LD.pt))
        {
            //Прорисуем яму
            GameObject TempPit = GameObject.Find("Pit_" + (OffsetX / (2 * LD.n + 3))
                + "_" + LD.Arr[i, j]);
            if (!TempPit.GetComponent<SpriteRenderer>().enabled)
            {
                TempPit.GetComponent<SpriteRenderer>().enabled = true;
                TempPit.GetComponentInChildren<MeshRenderer>().enabled = true;
            }
            OD = new ObjectsData(ObjectType.Pit, (OffsetX / (2 * LD.n + 3)), LD.Arr[i, j]);
            if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                //Добавляем в увиденные объекты
                SeenObjects[LD.CurrentPlayer - 1].Add(OD);
        }

        //Если динамит
        if (LD.Arr[i, j] == LD.pt * 2 + 2)
        {
            Rend = GameObject.Find("Dynamite_" + ((i - 1) / 2) + "_" +
                ((j - 1) / 2)).GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
            }
            OD = new ObjectsData(ObjectType.Dynamite,
                        Convert.ToInt32(((i - 1) / 2)), Convert.ToInt32((j - 1) / 2));
            if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                //Добавляем в увиденные объекты
                SeenObjects[LD.CurrentPlayer - 1].Add(OD);
        }


        //Если сокровище
        if ((LD.Arr[i, j] == LD.pt * 2 + 3) || (LD.Arr[i, j] == LD.pt * 2 + 4))
        {
            Rend = GameObject.Find("Treasure_" + ((i - 1) / 2) + "_" +
                ((j - 1) / 2)).GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
            }
            OD = new ObjectsData(ObjectType.Treasure,
                    Convert.ToInt32(((i - 1) / 2)), Convert.ToInt32((j - 1) / 2));
            if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                //Добавляем в увиденные объекты
                SeenObjects[LD.CurrentPlayer - 1].Add(OD);
        }


        //Если ловушка
        if (LD.Arr[i, j] == LD.pt * 2 + 5 || LD.Arr[i, j] == LD.pt * 2 + 7)
        {
            Rend = GameObject.Find("Spikes_" + ((i - 1) / 2) + "_" +
                ((j - 1) / 2)).GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
            }
            OD = new ObjectsData(ObjectType.Spikes,
                        Convert.ToInt32(((i - 1) / 2)), Convert.ToInt32((j - 1) / 2));
            if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                //Добавляем в увиденные объекты
                SeenObjects[LD.CurrentPlayer - 1].Add(OD);
        }

        //Если наступили на ускоритель
        if (LD.Arr[i, j] == LD.pt * 2 + 6 || LD.Arr[i, j] == LD.pt * 2 + 8)
        {
            Rend = GameObject.Find("SpeedUp_" + ((i - 1) / 2) + "_" +
                ((j - 1) / 2)).GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
            }
            OD = new ObjectsData(ObjectType.SpeedUp,
                        Convert.ToInt32(((i - 1) / 2)), Convert.ToInt32((j - 1) / 2));
            if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                //Добавляем в увиденные объекты
                SeenObjects[LD.CurrentPlayer - 1].Add(OD);
        }

        //Если фонарь
        if (LD.Arr[i, j] == LD.pt * 2 + 9)
        {
            Rend = GameObject.Find("Matches_" + ((i - 1) / 2) + "_" +
                ((j - 1) / 2)).GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
            }
            OD = new ObjectsData(ObjectType.Matches,
                        Convert.ToInt32(((i - 1) / 2)), Convert.ToInt32((j - 1) / 2));
            if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                //Добавляем в увиденные объекты
                SeenObjects[LD.CurrentPlayer - 1].Add(OD);
        }

        //Если паутина
        if (LD.Arr[i, j] == LD.pt * 2 + 10)
        {
            Rend = GameObject.Find("Web_" + ((i - 1) / 2) + "_" +
                ((j - 1) / 2)).GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
            }
            OD = new ObjectsData(ObjectType.Web,
                        Convert.ToInt32(((i - 1) / 2)), Convert.ToInt32((j - 1) / 2));
            if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                //Добавляем в увиденные объекты
                SeenObjects[LD.CurrentPlayer - 1].Add(OD);
        }
    }

    //Подбор объекта
    public static void Pick_Up()
    {
        int OffsetX;
        if (!LD.GameModeOneLabyrinth)
            if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 2))
            {
                OffsetX = 0;
            }
            else
            {
                if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 3) * 2 - 1)
                {
                    OffsetX = 2 * LD.n + 3;
                }
                else
                {
                    if (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < (2 * LD.n + 3) * 3 - 1)
                    {
                        OffsetX = (2 * LD.n + 3) * 2;
                    }
                    else
                    {
                        OffsetX = (2 * LD.n + 3) * 3;
                    }
                }
            }
        else
            OffsetX = 0;

        //Если не за пределами лабиринта, то проверяем, что под игроком
        if ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] < LD.m * 2) &&
             (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] > 0) &&
             (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] > OffsetX) &&
             (LD.PlayerPosition[LD.CurrentPlayer - 1, 0] < 2 * LD.n + OffsetX))
        {
            ObjectsData OD;

            //Если на динамит
            if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 2)
            {
                OD = new ObjectsData(ObjectType.Dynamite,
                            Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2)), Convert.ToInt32((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));

                if (Inventory_Interaction.IsInventoryFull())
                {
                    if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                        //Добавляем в увиденные объекты
                        SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                    Chat.Report("InventoryFull");
                }

                //Подбираем, если инвентарь не полон
                else
                {
                    if (Inventory_Interaction.CellClicked == 0)
                        //Находим, куда положить
                        for (int i = 0; i < LD.MaxNumberOfItems; i++)
                        {
                            if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.Nothing)
                            {
                                //Отображаем динамит в инвентаре
                                LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] = LD.InventoryObjects.Dynamite;
                                Inventory_Interaction.Upd_Inv();
                                Chat.Report("DynamitePickedUp");

                                SoundControlling.PlaySoundEffect("PickUp");

                                break;
                            }
                        }
                    else
                    {
                        if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] == LD.InventoryObjects.Nothing)
                        {
                            //Отображаем динамит в инвентаре
                            LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] = LD.InventoryObjects.Dynamite;
                            Inventory_Interaction.Upd_Inv();

                            SoundControlling.PlaySoundEffect("PickUp");
                        }
                    }

                    if (LD.GameModeTutorial)
                    {
                        if (Tutorial.Step == 23)
                        {
                            Tutorial.Step++;
                        }
                    }

                    DestroyGameObject("Dynamite", OD);
                }
            }

            //Если на сокровище
            if ((LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 3) || (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 4))
            {
                //Проверяем, есть ли в инвентаре сокровище (нельзя нести больше одного)
                bool HaveTreasure = false;
                for (int i = 0; i < LD.MaxNumberOfItems; i++)
                    if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.Treasure ||
                        LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.FalseTreasure)
                        HaveTreasure = true;

                OD = new ObjectsData(ObjectType.Treasure,
                        Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2)), Convert.ToInt32((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));

                if (HaveTreasure)
                    Chat.Report("HaveTreasure");

                if (Inventory_Interaction.IsInventoryFull())
                    Chat.Report("InventoryFull");

                if (HaveTreasure || Inventory_Interaction.IsInventoryFull())
                    if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                        SeenObjects[LD.CurrentPlayer - 1].Add(OD);

                //Подбираем, если инвентарь не полон
                if (!Inventory_Interaction.IsInventoryFull() && !HaveTreasure)
                {
                    if (Inventory_Interaction.CellClicked == 0)
                        //Находим, куда положить
                        for (int i = 0; i < LD.MaxNumberOfItems; i++)
                        {
                            if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.Nothing)
                            {
                                //Отображаем сокровище в инвентаре
                                if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 3)
                                    LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] = LD.InventoryObjects.Treasure;
                                else
                                    LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] = LD.InventoryObjects.FalseTreasure;
                                Inventory_Interaction.Upd_Inv();
                                Chat.Report("TreasurePickedUp");

                                SoundControlling.PlaySoundEffect("PickUp");

                                break;
                            }
                        }
                    else
                    {
                        if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] == LD.InventoryObjects.Nothing)
                        {
                            //Отображаем сокровище в инвентаре
                            if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 3)
                                LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] = LD.InventoryObjects.Treasure;
                            else
                                LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] = LD.InventoryObjects.FalseTreasure;
                            Inventory_Interaction.Upd_Inv();

                            SoundControlling.PlaySoundEffect("PickUp");
                        }
                    }

                    if (LD.GameModeTutorial)
                    {
                        if (Tutorial.Step == 23)
                        {
                            Tutorial.Step++;
                        }
                    }

                    DestroyGameObject("Treasure", OD);
                }
            }

            //Если стоим на фонаре
            if (LD.UnderThePlayer[LD.CurrentPlayer - 1] == LD.pt * 2 + 9)
            {
                OD = new ObjectsData(ObjectType.Matches,
                            Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2)), Convert.ToInt32((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));

                if (Inventory_Interaction.IsInventoryFull())
                {
                    if (!SeenObjects[LD.CurrentPlayer - 1].Contains(OD))
                        //Добавляем в увиденные объекты
                        SeenObjects[LD.CurrentPlayer - 1].Add(OD);
                    Chat.Report("InventoryFull");
                }

                //Подбираем, если инвентарь не полон
                else
                {
                    if (Inventory_Interaction.CellClicked == 0)
                        //Находим, куда положить
                        for (int i = 0; i < LD.MaxNumberOfItems; i++)
                        {
                            if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] == LD.InventoryObjects.Nothing)
                            {
                                //Отображаем фонарь в инвентаре
                                LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), i] = LD.InventoryObjects.Matches;
                                Inventory_Interaction.Upd_Inv();
                                Chat.Report("MatchesPickedUp");

                                SoundControlling.PlaySoundEffect("PickUp");

                                break;
                            }
                        }
                    else
                    {
                        if (LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] == LD.InventoryObjects.Nothing)
                        {
                            //Отображаем фонарь в инвентаре
                            LD.InInventory[(LD.GameModeCoop ? 0 : LD.CurrentPlayer - 1), Inventory_Interaction.CellClicked - 1] = LD.InventoryObjects.Matches;
                            Inventory_Interaction.Upd_Inv();

                            SoundControlling.PlaySoundEffect("PickUp");
                        }
                    }

                    if (LD.GameModeTutorial)
                    {
                        if (Tutorial.Step == 23)
                        {
                            Tutorial.Step++;
                        }
                    }

                    DestroyGameObject("Matches", OD);
                }
            }
        }
    }

    private static void DestroyGameObject(string GO, ObjectsData OD)
    {
        //Уничтожаем игровой объект
        LD.Arr[LD.PlayerPosition[LD.CurrentPlayer - 1, 0], LD.PlayerPosition[LD.CurrentPlayer - 1, 1]] = 0;
        Destroy(GameObject.Find(GO + "_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
            ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)));
        LD.UnderThePlayer[LD.CurrentPlayer - 1] = 0;
        for (int i = 0; i < LD.NumberOfPlayers; i++)
        {
            if (SeenObjects[i].Contains(OD))
                SeenObjects[i].Remove(OD);
            if (HiddenObjects[i].Contains(OD))
                HiddenObjects[i].Remove(OD);
        }
    }

    //Передача хода
    private IEnumerator Turn_Pass()
    {
        IsActionActive = true;
        //Выбор следующего игрока
        int NextPlayer = 1;
        for (int i = 0; i < LD.NumberOfPlayers; i++)
            if (LD.PlayerClock[NextPlayer - 1] > LD.PlayerClock[i])
                NextPlayer = i + 1;
            else
            {
                if (LD.PlayerClock[NextPlayer - 1] == LD.PlayerClock[i])
                    for (int k = 0; k < LD.NumberOfPlayers; k++)
                    {
                        if (LD.TurnOrder[k] == NextPlayer)
                            break;
                        if (LD.TurnOrder[k] == i + 1)
                        {
                            NextPlayer = i + 1;
                            break;
                        }
                    }
            }
        if (LD.CurrentPlayer != NextPlayer)
        {
            yield return new WaitForSeconds(0.3f);
            //Если не совместный режим - погасить объекты игрока и его самого
            if (!LD.GameModeCoop)
            {
                if (GameObject.Find("Detonation") != null)
                {
                    Destroy(GameObject.Find("Detonation"));
                }
                if (GameObject.Find("MineDetonation") != null)
                {
                    Destroy(GameObject.Find("MineDetonation"));
                }
                if (GameObject.Find("Light") != null)
                {
                    Destroy(GameObject.Find("Light"));
                }

                SeenListRenderChange(LD.CurrentPlayer, false);
                for (int i = 0; i < LD.NumberOfPlayers; i++)
                {
                    GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[0].enabled = false;
                    GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[1].enabled = false;
                }
            }
            GameObject.Find("Player_" + LD.CurrentPlayer).GetComponentsInChildren<SpriteRenderer>()[0].sortingOrder = 1;
            LD.CurrentPlayer = NextPlayer;

            if (!LD.GameModeOneLabyrinth)
            {
                if (LD.UnderThePlayer[LD.CurrentPlayer - 1] >= 2 * LD.pt + 11)
                {
                    EnterCaveButton.SetActive(true);
                }
                else
                {
                    EnterCaveButton.SetActive(false);
                }
            }

            if (LD.match == null)
            {
                Camera_Jump(LD.CurrentPlayer);
                UpdateTurnClock();
                for (int i = 0; i < LD.NumberOfPlayers; i++)
                {
                    if (SeenObjects[LD.CurrentPlayer - 1].Contains(new ObjectsData(ObjectType.FloorTile,
                        ((LD.PlayerPosition[i, 0] - 1) / 2), ((LD.PlayerPosition[i, 1] - 1) / 2))) ||
                        (i + 1) == LD.CurrentPlayer)
                    {
                        GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[0].enabled = true;
                        GameObject.Find("Player_" + (i + 1)).GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;
                    }
                }
                GameObject.Find("Player_" + LD.CurrentPlayer).GetComponentsInChildren<SpriteRenderer>()[0].sortingOrder = 2;
                if (!LD.GameModeCoop)
                {
                    //Включить все объекты текущего игрока
                    SeenListRenderChange(LD.CurrentPlayer, true);
                }
                if (LD.AskIfReady)
                {
                    WaitingPanel.SetActive(true);
                    if (LD.Ru)
                    {
                        GameObject.Find("WaitingText").GetComponent<Text>().text = "Игрок " + LD.CurrentPlayer + ", ваш ход";
                    }
                    else
                    {
                        GameObject.Find("WaitingText").GetComponent<Text>().text = "Player " + LD.CurrentPlayer + ", it's your turn";
                    }
                    SubmitButton.SetActive(true);
                    if (LD.Ru)
                    {
                        SubmitButton.GetComponentInChildren<Text>().text = "Готов!";
                    }
                    else
                    {
                        SubmitButton.GetComponentInChildren<Text>().text = "Go!";
                    }
                }
            }
            else
            {
                yield return StartCoroutine(Wait(0.8f));
                TryToSubmitTurn();
                CanPlay = false;
            }
        }
        Inventory_Interaction.Upd_Inv();
        Swipe = SwipeDirection.None;
        CoroutineToStart = "Move_Player";
        IsActionActive = false;
        yield break;
    }

    private void TryToSubmitTurn()
    {
        string GameStateString;
        GameState GS = new GameState(true);
        GameStateString = GS.SaveToString();
        MyData = new byte[Encoding.Default.GetByteCount(GameStateString)];
        MyData = Encoding.Default.GetBytes(GameStateString);
        string WhoIsNext = DecideWhoIsNext();
        WaitingPanel.SetActive(true);
        if (LD.Ru)
        {
            GameObject.Find("WaitingText").GetComponent<Text>().text = "Отправка вашего хода, подождите.";
        }
        else
        {
            GameObject.Find("WaitingText").GetComponent<Text>().text = "Submitting your turn, please wait.";
        }
        SubmitButton.SetActive(false);
        GameObject.Find("BackButton").GetComponent<Button>().interactable = false;
        PlayGamesPlatform.Instance.TurnBased.TakeTurn(LD.match, MyData, WhoIsNext, (bool success) =>
        {
            if (success)
            {
                if (LD.Ru)
                {
                    GameObject.Find("WaitingText").GetComponent<Text>().text = "Ход успешно отправлен.";
                }
                else
                {
                    GameObject.Find("WaitingText").GetComponent<Text>().text = "Turn submited successfully.";
                }
                SubmitButton.SetActive(false);
                GameObject.Find("BackButton").GetComponent<Button>().interactable = false;
                StartCoroutine("WaitAndBackToOnline");
            }
            else
            {
                if (LD.Ru)
                {
                    GameObject.Find("WaitingText").GetComponent<Text>().text = "Не удалось отправить ваш ход, попробуйте ещё раз.";
                    SubmitButton.GetComponentInChildren<Text>().text = "Отправить";
                }
                else
                {
                    GameObject.Find("WaitingText").GetComponent<Text>().text = "An error occured. Can't submit your turn. Try again.";
                    SubmitButton.GetComponentInChildren<Text>().text = "Submit";
                }
                SubmitButton.SetActive(true);
                GameObject.Find("BackButton").GetComponent<Button>().interactable = true;
            }
        });
    }

    private string DecideWhoIsNext()
    {
        if (LD.match.AvailableAutomatchSlots > 0)
        {
            // hand over to an automatch player
            return null;
        }
        else
        {
            // pick a player among match.Participants,
            // according to the game's logic
            return LD.match.Participants[LD.CurrentPlayer - 1].ParticipantId;
        }
    }

    IEnumerator WaitAndBackToOnline()
    {
        yield return StartCoroutine(Wait(1.0f));
        SceneManager.LoadSceneAsync("OnlineRoom");
        yield break;
    }

    public void RetrySubmitting()
    {
        if (LD.match != null)
        {
            CanPlay = false;
            GameObject.Find("BackButton").GetComponent<Button>().interactable = false;
            TryToSubmitTurn();
        }
        else
        {
            WaitingPanel.SetActive(false);
        }
    }

    public static void IncTurnClock()
    {
        LD.PlayerClock[LD.CurrentPlayer - 1] += LD.TurnDuration[LD.CurrentPlayer - 1];
        if (Turns[LD.CurrentPlayer - 1] <= 10000.0f)
            Turns[LD.CurrentPlayer - 1] += LD.TurnDuration[LD.CurrentPlayer - 1];
        //Откат часов, если все больше 10
        bool AllGreater = true;
        for (int i = 0; i < LD.NumberOfPlayers; i++)
            if (LD.PlayerClock[LD.TurnOrder[i] - 1] <= LD.m)
                AllGreater = false;
        if (AllGreater)
        {
            if (LD.match == null)
            {
                GameObject.Find("BackButton").GetComponent<Button>().interactable = false;
                if (LD.Ru)
                {
                    Chat.AddNewText("Автосохранение...", Color.white);
                }
                else
                {
                    Chat.AddNewText("Autosaving...", Color.white);
                }
                GameState GS = new GameState(true);
                PlayerPrefs.SetString("GameState", GS.SaveToString());
                GameObject.Find("BackButton").GetComponent<Button>().interactable = true;
            }
            for (int i = 0; i < LD.NumberOfPlayers; i++)
            {
                LD.PlayerClock[LD.TurnOrder[i] - 1] -= LD.m;
            }
        }
        UpdateTurnClock();
    }

    private static void UpdateTurnClock()
    {
        if (Turns[LD.CurrentPlayer - 1] <= 10000.0f)
            GameObject.Find("TurnCounter").GetComponent<Text>().text = (LD.Ru ? "Ходов" : "Turns") + ": " + Turns[LD.CurrentPlayer - 1];
        else
            GameObject.Find("TurnCounter").GetComponent<Text>().text = (LD.Ru ? "Ходов" : "Turns") + ": 10000+";
        GameObject.Find("TurnCounter").GetComponent<Text>().color = LD.PlayerColors[LD.CurrentPlayer - 1];
    }

}