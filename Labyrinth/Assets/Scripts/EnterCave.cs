﻿using System;
using System.Collections;
using UnityEngine;

public class EnterCave : MonoBehaviour
{
    public void EnterClicked()
    {
        StartCoroutine(Enter());
    }

	public IEnumerator Enter()
    {
        if (LD.UnderThePlayer[LD.CurrentPlayer - 1] >= 2 * LD.pt + 11)
        {
            SoundControlling.PlaySoundEffect("Cave");

            int GoToLabyrinthNumber = LD.UnderThePlayer[LD.CurrentPlayer - 1] - (2 * LD.pt + 11),
            CurrentLabyrinthNumber = LD.PlayerPosition[LD.CurrentPlayer - 1, 0] / (2 * LD.n + 3),
            Difference = GoToLabyrinthNumber - CurrentLabyrinthNumber;

            Chat.Report("CaveEntered", GoToLabyrinthNumber + 1);

            LD.UnderThePlayer[LD.CurrentPlayer - 1] = 2 * LD.pt + 11 + CurrentLabyrinthNumber;
            //Сохраним новые координаты
            LD.PlayerPosition[LD.CurrentPlayer - 1, 0] += (2 * LD.n + 3) * Difference;
            Vector3 NewPos = new Vector3(-0.5f + LD.PlayerPosition[LD.CurrentPlayer-1,0] * 0.5f,
                -0.5f + LD.PlayerPosition[LD.CurrentPlayer - 1, 1] * 0.5f, 0);

            //Перебрасываем
            yield return StartCoroutine(Player_Movement.MovePlayerAndCamera(NewPos, true));

            //Прорисуем тайл пола, на который переброшены и добавим в увиденные обхекты
            GameObject.Find("FloorTile_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
                ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)).GetComponent<SpriteRenderer>().enabled = true;
            Player_Movement.SeenObjects[LD.CurrentPlayer - 1].Add(new Player_Movement.ObjectsData(Player_Movement.ObjectType.FloorTile,
                ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2), (LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));

            //Прорисуем выход и добавим в увиденные обхекты
            SpriteRenderer Rend = GameObject.Find("Cave_" + ((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2) + "_" +
                        ((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2)).GetComponent<SpriteRenderer>();
            if (!Rend.enabled)
            {
                Rend.enabled = true;
            }

            Player_Movement.ObjectsData OD = new Player_Movement.ObjectsData(Player_Movement.ObjectType.Cave,
                    Convert.ToInt32(((LD.PlayerPosition[LD.CurrentPlayer - 1, 0] - 1) / 2)), Convert.ToInt32((LD.PlayerPosition[LD.CurrentPlayer - 1, 1] - 1) / 2));
            Player_Movement.SeenObjects[LD.CurrentPlayer - 1].Add(OD);

            Player_Movement.IncTurnClock();
            if (LD.NumberOfPlayers > 1)
                Player_Movement.CoroutineToStart = "Turn_Pass";
            else
                Player_Movement.IsActionActive = false;

            yield break;
        }
	}
}
