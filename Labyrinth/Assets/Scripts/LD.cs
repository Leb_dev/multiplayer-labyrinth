using GooglePlayGames.BasicApi.Multiplayer;
using UnityEngine;

//����� � ����������� �����������
public class LD : MonoBehaviour
{

    public const int MaxNumberOfPlayers = 4, MaxNumberOfItems = 4;
    public const int MaxM = 40, MaxN = 40;
    public const int MaxVisibleChatTexts = 4, MaxChatTexts = 16;
    public const int MaxXSize = (2 * MaxN + 1) * MaxNumberOfPlayers + 2 * (MaxNumberOfPlayers - 1);

    public static System.Random Seeder = new System.Random(); //�����������

    //������, ������, ���������� ��, ���������� ���������� �����, ������ ��������
    public static int m = 40, n = 40, pt = 36, grn = 18,
        flstr = 15, tr = 4;
    public static int CurrentPlayer, NumberOfPlayers = 1;  //���������� ������� � ������� (1-4)

    public static int[] UnderThePlayer = new int[MaxNumberOfPlayers] { 0, 0, 0, 0 }; //��� ��� �������
    public static int[] TurnOrder = new int[MaxNumberOfPlayers]; //��������� ������� �����

    public static int[,] Arr = new int[MaxXSize, 2 * MaxM + 1];   //������ ���������
    public static int[,] PlayerPosition = new int[MaxNumberOfPlayers, 2];    //[������������� ������; ������� ������: [0] - x, [1] - y]

    public static float[] PlayerClock = new float[MaxNumberOfPlayers];    //"����" ������
    public static float[] TurnDuration = new float[MaxNumberOfPlayers]; //������������ ����

    public enum InventoryObjects
    {
        Nothing, Dynamite, Treasure, FalseTreasure, Matches
    }
    public static InventoryObjects[,] InInventory = new InventoryObjects[MaxNumberOfPlayers, MaxNumberOfItems];  //�����, ������

    public static Color[] PlayerColors = new Color[MaxNumberOfPlayers]
    {new Color(1f,0.4f,0.2f), new Color(0.8f,0.75f,0.1f), new Color(0.55f,0.9f,0.2f), new Color(0.86f,0.35f,0.95f) };

    public static string[] ChatTexts = new string[MaxChatTexts];
    public static Color[] ChatTextsColors = new Color[MaxChatTexts];

    public static bool GameModeCoop = true; //������� �����
    public static bool GameModeOneLabyrinth = true; //������� �����
    public static bool GameModeTutorial = false; //������� �����
    public static bool Authorized;
    public static bool Ru = false;
    public static bool AskIfReady;
    public static bool GameLoading = false;

    public static TurnBasedMatch match;

}