﻿using UnityEngine;

public class SelfTurnOff : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (gameObject.name == "FingerDragFourSides")
        {
            if (Player_Movement.Swipe != Player_Movement.SwipeDirection.None && Tutorial.Step == 23)
            {
                gameObject.SetActive(false);
            }
        }
    }

    private void OnDisable()
    {
        if (gameObject.name == "FingerClickClear" && LD.GameModeTutorial)
        {
            Player_Movement.FingerClickPick.SetActive(true);
        }
    }

}