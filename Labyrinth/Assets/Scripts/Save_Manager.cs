﻿using UnityEngine;

public class Save_Manager : MonoBehaviour
{

    //PlayerPrefs хранятся в HKCU\Software\[company name]\[product name]
    public void Save_Clicked()
    {
        if (!Player_Movement.IsActionActive)
        {
            Player_Movement.IsActionActive = true;
            if (LD.Ru)
            {
                Chat.AddNewText("Голос в Вашей голове: \"Сохранение... Пожалуйста, не закрывайте игру\".", Color.white);
            }
            else
            {
                Chat.AddNewText("A voice in your head: \"Now saving... Please, don't close the game\".", Color.white);
            }

            SoundControlling.PlaySoundEffect("Saving");

            SaveGame();
        }
    }

    public static void SaveGame()
    {
        GameState GS = new GameState(true);
        PlayerPrefs.SetString("GameState", GS.SaveToString());
        if (LD.Ru)
        {
            Chat.AddNewText("Голос в Вашей голове: \"Успешно сохранено!\"", Color.white);
        }
        else
        {
            Chat.AddNewText("A voice in your head: \"Progress saved successfully!\"", Color.white);
        }
        Player_Movement.IsActionActive = false;
    }

    public static void LoadGame()
    {
        if (PlayerPrefs.HasKey("GameState"))
        {
            GameState GS = new GameState(false);
            GS = GS.LoadFromString(PlayerPrefs.GetString("GameState"));
            GS.LoadValues();
        }
    }

    public static void DeleteSave()
    {
        if (PlayerPrefs.HasKey("GameState"))
        {
            PlayerPrefs.DeleteKey("GameState");
        }
    }

}