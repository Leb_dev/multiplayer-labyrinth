﻿using System.Collections.Generic;
using UnityEngine;

public class GameState
{

    //Высота, ширина, количество ям, количество динамитных шашек, ложных сокровищ
    public int m, n, pt, grn, flstr, tr;
    public int CurrentPlayer, NumberOfPlayers;  //Количество игроков и текущий (1-4)

    public int[] UnderThePlayer; //Что под игроком
    public int[] TurnOrder; //Дефолтный порядок ходов
    public int[] InInventoryInInt;
    public int[] SeenCount, HiddenCount;

    public bool GameModeCoop; //Игровой режим
    public bool GameModeOneLabyrinth; //Игровой режим

    public float[] PlayerClock;    //"Часы" ироков
    public float[] Turns;   //Счётчик ходов игроков
    public float[] TurnDuration; //Длительность хода

    public string[] ChatTexts;

    public Vector2[] PlayerPositionInVector;

    public Vector3[] ArrInVector;
    public Vector3[] SeenObjectsInVector, HiddenObjectsInVector;

    public string SaveToString()
    {
        return JsonUtility.ToJson(this);
    }
    
    public GameState LoadFromString(string myData)
    {
        return JsonUtility.FromJson<GameState>(myData);
    }

    public void LoadValues()
    {
        LD.m = m;
        LD.n = n;
        LD.pt = pt;
        LD.grn = grn;
        LD.flstr = flstr;
        LD.tr = tr;

        for (int j = 0; j < 2 * m + 1; j++)
            for (int i = 0; i < LD.MaxXSize; i++)
                LD.Arr[i, j] = 0;

        foreach (Vector3 Vector in ArrInVector)
        {
            if (Vector.x != 0 || Vector.y != 0 || Vector.z != 0)
            {
                LD.Arr[(int)Vector.x, (int)Vector.y] = (int)Vector.z;
            }
        }

        for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
        {
            LD.PlayerPosition[i, 0] = (int)PlayerPositionInVector[i].x;
            LD.PlayerPosition[i, 1] = (int)PlayerPositionInVector[i].y;
        }
        for (int i = 0; i < LD.NumberOfPlayers; i++)
        {
            LD.UnderThePlayer[i] = UnderThePlayer[i];
        }

        LD.CurrentPlayer = CurrentPlayer;
        LD.NumberOfPlayers = NumberOfPlayers;
        LD.GameModeCoop = GameModeCoop;
        LD.GameModeOneLabyrinth = GameModeOneLabyrinth;

        for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
        {
            LD.PlayerClock[i] = PlayerClock[i];
            LD.TurnOrder[i] = TurnOrder[i];
            LD.TurnDuration[i] = TurnDuration[i];
            Player_Movement.Turns[i] = Turns[i];
        }

        for (int i = 0; i < LD.MaxNumberOfPlayers * LD.MaxNumberOfItems; i++)
        {
            LD.InInventory[i / LD.MaxNumberOfPlayers, i % LD.MaxNumberOfItems] = (LD.InventoryObjects)InInventoryInInt[i];
        }

        if (Player_Movement.SeenObjects[0]==null || Player_Movement.HiddenObjects[0]==null)
        {
            for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
            {
                Player_Movement.SeenObjects[i] = new List<Player_Movement.ObjectsData>();
                Player_Movement.HiddenObjects[i] = new List<Player_Movement.ObjectsData>();
            }
        }
        for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
        {
            Player_Movement.SeenObjects[i].Clear();
            Player_Movement.HiddenObjects[i].Clear();
        }
        int Pl = 0;
        for (int i = 0; i < SeenObjectsInVector.Length; i++)
        {
            noinci:
            if ((SeenCount[Pl]--) != 0)
            {
                Player_Movement.ObjectsData OD = new Player_Movement.ObjectsData(
                    (Player_Movement.ObjectType)(int)SeenObjectsInVector[i].x, (int)SeenObjectsInVector[i].y, (int)SeenObjectsInVector[i].z);
                Player_Movement.SeenObjects[Pl].Add(OD);
            }
            else
            {
                Pl++;
                goto noinci;
            }
        }
        Pl = 0;
        for (int i = 0; i < HiddenObjectsInVector.Length; i++)
        {
            noinci:
            if ((HiddenCount[Pl]--) != 0)
            {
                Player_Movement.ObjectsData OD = new Player_Movement.ObjectsData(
                    (Player_Movement.ObjectType)(int)HiddenObjectsInVector[i].x, (int)HiddenObjectsInVector[i].y, (int)HiddenObjectsInVector[i].z);
                Player_Movement.HiddenObjects[Pl].Add(OD);
            }
            else
            {
                Pl++;
                goto noinci;
            }
        }
        for (int i = 0; i < LD.MaxChatTexts; i++)
        {
            LD.ChatTexts[i] = ChatTexts[i];
        }
    }

    public GameState(bool SaveValues)
    {
        if (SaveValues)
        {
            m = LD.m;
            n = LD.n;
            pt = LD.pt;
            grn = LD.grn;
            flstr = LD.flstr;
            tr = LD.tr;

            int XSize = (LD.GameModeOneLabyrinth? (2 * n + 1) : ((2 * n + 1) * LD.NumberOfPlayers + 2 * (LD.NumberOfPlayers - 1))),
                YSize = (2 * m + 1);
            ArrInVector = new Vector3[XSize * YSize];

            int index = 0;
            for (int j = 0; j < YSize; j++)
                for (int i = 0; i < XSize; i++)
                    ArrInVector[index++] = new Vector3(i, j, LD.Arr[i, j]);

            PlayerPositionInVector = new Vector2[LD.MaxNumberOfPlayers];
            for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
            {
                PlayerPositionInVector[i] = new Vector2(LD.PlayerPosition[i, 0], LD.PlayerPosition[i, 1]);
            }

            UnderThePlayer = new int[LD.MaxNumberOfPlayers];
            for (int i = 0; i < LD.NumberOfPlayers; i++)
            {
                UnderThePlayer[i] = LD.UnderThePlayer[i];
            }

            CurrentPlayer = LD.CurrentPlayer;
            NumberOfPlayers = LD.NumberOfPlayers;
            GameModeCoop = LD.GameModeCoop;
            GameModeOneLabyrinth = LD.GameModeOneLabyrinth;

            PlayerClock = new float[LD.MaxNumberOfPlayers];
            TurnOrder = new int[LD.MaxNumberOfPlayers];
            TurnDuration = new float[LD.MaxNumberOfPlayers];
            Turns = new float[LD.MaxNumberOfPlayers];
            for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
            {
                PlayerClock[i] = LD.PlayerClock[i];
                TurnOrder[i] = LD.TurnOrder[i];
                TurnDuration[i] = LD.TurnDuration[i];
                Turns[i] = Player_Movement.Turns[i];
            }

            InInventoryInInt = new int[LD.MaxNumberOfPlayers * LD.MaxNumberOfItems];

            for (int i = 0; i < LD.MaxNumberOfPlayers * LD.MaxNumberOfItems; i++)
            {
                InInventoryInInt[i] = (int)LD.InInventory[i / LD.MaxNumberOfPlayers, i % LD.MaxNumberOfItems];
            }

            int Cnt = 0;
            for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
            {
                Cnt += Player_Movement.SeenObjects[i].Count;
            }
            SeenObjectsInVector = new Vector3[Cnt];
            Cnt = 0;
            for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
            {
                Cnt += Player_Movement.HiddenObjects[i].Count;
            }
            HiddenObjectsInVector = new Vector3[Cnt];
            SeenCount = new int[LD.MaxNumberOfPlayers];
            HiddenCount = new int[LD.MaxNumberOfPlayers];

            int index1 = 0, index2 = 0;
            for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
            {
                for (int j = 0; j < Player_Movement.SeenObjects[i].Count; j++)
                {
                    SeenObjectsInVector[index1++] = new Vector3((int)Player_Movement.SeenObjects[i][j].Obj,
                        Player_Movement.SeenObjects[i][j].x,
                        Player_Movement.SeenObjects[i][j].y);
                }
                for (int k = 0; k < Player_Movement.HiddenObjects[i].Count; k++)
                {
                    HiddenObjectsInVector[index2++] = new Vector3((int)Player_Movement.HiddenObjects[i][k].Obj,
                        Player_Movement.HiddenObjects[i][k].x,
                        Player_Movement.HiddenObjects[i][k].y);
                }
                SeenCount[i] = Player_Movement.SeenObjects[i].Count;
                HiddenCount[i] = Player_Movement.HiddenObjects[i].Count;
            }
            ChatTexts = new string[LD.MaxChatTexts];
            for (int i = 0; i < LD.MaxChatTexts; i++)
            {
                ChatTexts[i] = LD.ChatTexts[i];
            }
        }
    }

}