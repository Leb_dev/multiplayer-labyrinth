﻿using UnityEngine;
using System;

public class Labyrinth_Generation : MonoBehaviour
{

    private int m = LD.m, n = LD.n, pt = LD.pt, OffsetX; //Высота, Ширина, Количество ям

    public void Start()
    {
        if (!LD.GameLoading)
            GenerateNewLabyrinth();
        else
            InstantiateObjects();
    }

    private void GenerateNewLabyrinth()
    {
        for (int i = 0; i < LD.MaxNumberOfPlayers; i++)
        {
            LD.PlayerPosition[i, 0] = 0;
            LD.PlayerPosition[i, 1] = 0;
            LD.UnderThePlayer[i] = 0;
        }


        //----------ГЕНЕРАЦИЯ ЛАБИРИНТА----------

        //Обнуляем массив
        for (int j = 0; j < 2 * m + 1; j++)
            for (int i = 0; i < LD.MaxXSize; i++)
                LD.Arr[i, j] = 0;

        //Подготовка поля
        for (OffsetX = 0; OffsetX < (!LD.GameModeOneLabyrinth ? LD.NumberOfPlayers * (2 * n + 3) : 1); OffsetX += 2 * n + 3)
        {
            bool OkFlag = false;    //Если true, то лабиринт сгенерирован нормально
            do
            {
                //Помещаем в крайние и узловые клетки 1 (стена)
                for (int j = 0; j <= 2 * m; j++)
                    for (int i = OffsetX; i <= (2 * n) + OffsetX; i++)
                        if (((i % 2 == 0 && j % 2 == 0 && OffsetX % 2 == 0) ||
                            (i % 2 != 0 && j % 2 == 0 && OffsetX % 2 != 0)) ||
                            (i == OffsetX) || (j == 0) ||
                            (i == (2 * n) + OffsetX) || (j == 2 * m))
                            LD.Arr[i, j] = 1;

                //Присваиваем каждой пустой клетке уникальный номер
                int Element = 2;
                for (int j = 1; j < 2 * m; j += 2)
                    for (int i = 1 + OffsetX; i < (2 * n) + OffsetX; i += 2)
                        LD.Arr[i, j] = Element++;

                //Генерация стен лабиринта
                for (int j = 1; j < 2 * m; j += 2)
                    Generate(j);    //Магия

                //Обнуление клеток, где нет стен (не мусорим, господа)
                for (int i = 1 + OffsetX; i < (2 * n) + OffsetX; i += 1)
                    for (int j = 1; j < 2 * m; j += 1)
                        if (LD.Arr[i, j] != 1)
                            LD.Arr[i, j] = 0;

                //Удаляем 4 стены для выходов
                PlaceExits();

                //Размещаем ловушки
                PlacePits(ref OkFlag);

            } while (!OkFlag);

            //Установить позицию игрока в массиве
            if (LD.GameModeOneLabyrinth)
                PlaceOnClear(LD.NumberOfPlayers, 1 + 2 * pt);
            else
                PlaceOnClear(1, 1 + 2 * pt);

            //Разместить динамит, фонари и паутину в массиве
            PlaceOnClear(LD.grn, pt * 2 + 2);
            PlaceOnClear(LD.grn, pt * 2 + 9);
            PlaceOnClear(LD.grn, pt * 2 + 10);

            //Разместить в массиве сокровища: 1 подлинное и несколько ложных
            PlaceOnClear(1, pt * 2 + 3);
            if (LD.flstr != 0)
                PlaceOnClear(LD.flstr, pt * 2 + 4);

            //Разместить ловушки в массиве
            PlaceOnClear(LD.tr, pt * 2 + 5);

            //Разместить ускорители в массиве
            PlaceOnClear(LD.tr, pt * 2 + 6);
            
            //----------КОНЕЦ ГЕНЕРАЦИИ ЛАБИРИНТА----------
        }
        
        if (!LD.GameModeOneLabyrinth)
        {
            for (int i = 0; i < LD.NumberOfPlayers; i++)
            {
                for (int j = i; j < LD.NumberOfPlayers; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }
                        bool Placed;
                    do
                    {
                        Placed = false;
                        int PositionX = LD.Seeder.Next(1, n + 1),
                        PositionX1 = PositionX + (2 * n + 3) * i,
                        PositionX2 = PositionX + (2 * n + 3) * j,
                        PositionY = LD.Seeder.Next(1, m + 1) * 2 - 1;
                        if (LD.Arr[PositionX1, PositionY] == 0 && LD.Arr[PositionX2, PositionY] == 0
                            && (PositionX % 2 != 0))
                        {
                            LD.Arr[PositionX1, PositionY] = j + (2 * pt + 11);
                            LD.Arr[PositionX2, PositionY] = i + (2 * pt + 11);
                            Placed = true;
                        }
                    } while (!Placed);
                }
            }
        }

        InstantiateObjects();
    }

    private void InstantiateObjects()
    {
        //Загрузка префабов для инициализации объектов
        GameObject Wall = Resources.Load<GameObject>("Prefabs/Wall");
        GameObject Player = Resources.Load<GameObject>("Prefabs/Player");
        GameObject Pit = Resources.Load<GameObject>("Prefabs/PitIn");
        GameObject PitExit = Resources.Load<GameObject>("Prefabs/PitOut");
        GameObject FloorTile = Resources.Load<GameObject>("Prefabs/Floor_Tile");
        GameObject Dynamite = Resources.Load<GameObject>("Prefabs/Dynamite");
        GameObject Treasure = Resources.Load<GameObject>("Prefabs/Treasure");
        GameObject SpeedUp = Resources.Load<GameObject>("Prefabs/SpeedUp");
        GameObject Spikes = Resources.Load<GameObject>("Prefabs/Spikes");
        GameObject Matches = Resources.Load<GameObject>("Prefabs/Matches");
        GameObject Web = Resources.Load<GameObject>("Prefabs/Web");
        GameObject Cave = Resources.Load<GameObject>("Prefabs/Cave");

        GameObject TempObj;
        Quaternion ZeroQuaternion = new Quaternion(0, 0, 0, 0);

        Player_Movement.FloorTiles = new GameObject[164, 40];
        Player_Movement.Walls = new GameObject[330, 81];

        int Depth;
        bool Visible = false;

        for (OffsetX = 0; OffsetX < (!LD.GameModeOneLabyrinth ? LD.NumberOfPlayers * (2 * n + 3) : 1); OffsetX += 2 * n + 3)
        {
            Depth = -1;
            //Размещение объектов на игровом поле, согласно массиву
            for (int j = 2 * m; j >= 0; j--)
            {
                Depth++;
                for (int i = OffsetX; i <= (2 * n) + OffsetX; i++)
                {
                    //Размещаем вертикальные стены
                    if ((i % 2 == 0 && LD.Arr[i, j] == 1 && OffsetX % 2 == 0) ||
                        (i % 2 != 0 && LD.Arr[i, j] == 1 && OffsetX % 2 != 0))
                    {
                        if (j % 2 != 0)
                        {
                            Sprite WallSprite = Resources.Load<Sprite>("Sprites/Walls/WallVertical" + LD.Seeder.Next(1, 7));
                            Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                            TempObj = Instantiate(Wall, TempPos, ZeroQuaternion);
                            TempObj.name = "Wall_" + i + "_" + j;
                            TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                            TempObj.GetComponent<SpriteRenderer>().sprite = WallSprite;
                            TempObj.GetComponent<SpriteRenderer>().sortingOrder = Depth;
                            Player_Movement.Walls[i, j] = TempObj;
                        }
                    }
                    else
                    {
                        //Размещаем горизонтальные стены
                        if ((j % 2 == 0) && (LD.Arr[i, j] == 1))
                        {
                            Sprite WallSprite = Resources.Load<Sprite>("Sprites/Walls/WallHorizontal" + LD.Seeder.Next(1, 7));
                            Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                            TempObj = Instantiate(Wall, TempPos, ZeroQuaternion);
                            TempObj.name = "Wall_" + i + "_" + j;
                            TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                            TempObj.GetComponent<SpriteRenderer>().sprite = WallSprite;
                            TempObj.GetComponent<SpriteRenderer>().sortingOrder = Depth;
                            Player_Movement.Walls[i, j] = TempObj;
                        }
                    }

                    //Разместим игрока
                    if (LD.Arr[i, j] == 1 + 2 * pt)
                    {
                        int PlayerPlacing;
                        if (LD.GameModeOneLabyrinth)
                            do
                            {
                                PlayerPlacing = LD.Seeder.Next(1, LD.NumberOfPlayers + 1);
                            } while (GameObject.Find("Player_" + PlayerPlacing) != null);
                        else
                        {
                            PlayerPlacing = (OffsetX / (2 * n + 3)) + 1;
                        }
                        LD.PlayerPosition[PlayerPlacing - 1, 0] = i;
                        LD.PlayerPosition[PlayerPlacing - 1, 1] = j;
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(Player, TempPos, ZeroQuaternion);
                        TempObj.name = "Player_" + PlayerPlacing;
                        TempObj.GetComponentsInChildren<SpriteRenderer>()[0].color = LD.PlayerColors[PlayerPlacing - 1];
                        TempObj.GetComponentsInChildren<SpriteRenderer>()[0].enabled = false;
                        TempObj.GetComponentsInChildren<SpriteRenderer>()[1].enabled = false;
                        LD.Arr[i, j] = 0;
                        LD.UnderThePlayer[PlayerPlacing - 1] = 0;
                    }

                    //Ямы
                    if ((LD.Arr[i, j] > 0 && LD.Arr[i, j] <= pt && i % 2 != 0 && j % 2 != 0 && OffsetX % 2 == 0) ||
                        (LD.Arr[i, j] > 0 && LD.Arr[i, j] <= pt && i % 2 == 0 && j % 2 != 0 && OffsetX % 2 != 0))
                    {
                        string[] RomanNubers = {"I","II","III","IV","V","VI","VII","VIII",
                                    "IX","X","XI","XII","XIII","XIV","XV","XVI","XVII","XVIII","XIX","XX",
                                    "XXI","XXII","XXIII","XXIV","XXV","XXVI","XXVII","XXVIII","XXIX",
                                    "XXX","XXXI","XXXII","XXXIII","XXXIV","XXXV","XXXVI"};
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(Pit, TempPos, ZeroQuaternion);
                        TempObj.name = "Pit_" + (OffsetX / (2 * n + 3)) + "_" + LD.Arr[i, j];
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                        TempObj.GetComponentInChildren<TextMesh>().text = RomanNubers[LD.Arr[i, j] - 1];
                        TempObj.GetComponentInChildren<MeshRenderer>().enabled = Visible;
                    }

                    //Выходы из ям
                    if ((LD.Arr[i, j] > pt) && (LD.Arr[i, j] <= pt * 2))
                    {
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(PitExit, TempPos, ZeroQuaternion);
                        TempObj.name = "PitExit_" + (OffsetX / (2 * n + 3)) + "_" + (LD.Arr[i, j] - pt);
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                        TempObj.GetComponentInChildren<TextMesh>().text = Convert.ToString(LD.Arr[i, j] - pt);
                        TempObj.GetComponentInChildren<MeshRenderer>().enabled = Visible;
                    }

                    //Динамит
                    if (LD.Arr[i, j] == pt * 2 + 2)
                    {
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(Dynamite, TempPos, ZeroQuaternion);
                        TempObj.name = "Dynamite_" + ((i - 1) / 2) + "_" + ((j - 1) / 2);
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                    }

                    //Сокровища
                    if ((LD.Arr[i, j] == pt * 2 + 3) || (LD.Arr[i, j] == pt * 2 + 4))
                    {
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(Treasure, TempPos, ZeroQuaternion);
                        TempObj.name = "Treasure_" + ((i - 1) / 2) + "_" + ((j - 1) / 2);
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                    }

                    //Ловушки
                    if (LD.Arr[i, j] == pt * 2 + 5)
                    {
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(Spikes, TempPos, ZeroQuaternion);
                        TempObj.name = "Spikes_" + ((i - 1) / 2) + "_" + ((j - 1) / 2);
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                    }

                    //Ускорители
                    if (LD.Arr[i, j] == pt * 2 + 6)
                    {
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(SpeedUp, TempPos, ZeroQuaternion);
                        TempObj.name = "SpeedUp_" + ((i - 1) / 2) + "_" + ((j - 1) / 2);
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                    }

                    //Ловушки
                    if (LD.Arr[i, j] == pt * 2 + 7)
                    {
                        Sprite TriggeredSpikes = Resources.Load<Sprite>("Sprites/SpikesTriggered");
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(Spikes, TempPos, ZeroQuaternion);
                        TempObj.name = "Spikes_" + ((i - 1) / 2) + "_" + ((j - 1) / 2);
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                        TempObj.GetComponent<SpriteRenderer>().sprite = TriggeredSpikes;
                    }

                    //Ускорители
                    if (LD.Arr[i, j] == pt * 2 + 8)
                    {
                        Sprite TriggeredSpeedUp = Resources.Load<Sprite>("Sprites/SpeedUpTriggered");
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(SpeedUp, TempPos, ZeroQuaternion);
                        TempObj.name = "SpeedUp_" + ((i - 1) / 2) + "_" + ((j - 1) / 2);
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                        TempObj.GetComponent<SpriteRenderer>().sprite = TriggeredSpeedUp;
                    }

                    //Cпички
                    if (LD.Arr[i, j] == pt * 2 + 9)
                    {
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(Matches, TempPos, ZeroQuaternion);
                        TempObj.name = "Matches_" + ((i - 1) / 2) + "_" + ((j - 1) / 2);
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                    }

                    //Паутину
                    if (LD.Arr[i, j] == pt * 2 + 10)
                    {
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(Web, TempPos, ZeroQuaternion);
                        TempObj.name = "Web_" + ((i - 1) / 2) + "_" + ((j - 1) / 2);
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                    }

                    //Пещеры
                    if (LD.Arr[i, j] >= pt * 2 + 11)
                    {
                        Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                        TempObj = Instantiate(Cave, TempPos, ZeroQuaternion);
                        TempObj.name = "Cave_" + ((i - 1) / 2) + "_" + ((j - 1) / 2);
                        TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                        TempObj.GetComponentInChildren<TextMesh>().text = (LD.Arr[i, j] - (pt * 2 + 10)).ToString();
                        TempObj.GetComponentInChildren<MeshRenderer>().enabled = Visible;
                    }
                }
            }

            //Размещаем тайлы пола
            for (int j = 1; j < 2 * m; j += 2)
                for (int i = 1 + OffsetX; i < 2 * n + OffsetX; i += 2)
                {
                    Vector3 TempPos = new Vector3(-0.5f + i * 0.5f, -0.5f + j * 0.5f, 0);
                    TempObj = Instantiate(FloorTile, TempPos, ZeroQuaternion);
                    TempObj.name = "FloorTile_" + ((i - 1) / 2) + "_" + ((j - 1) / 2);
                    TempObj.GetComponent<SpriteRenderer>().enabled = Visible;
                    Player_Movement.FloorTiles[((i - 1) / 2), ((j - 1) / 2)] = TempObj;
                }
        }

        for (int i = 1; i <= LD.NumberOfPlayers; i++)
        {
            if (GameObject.Find("Player_" + i) == null)
            {
                Vector3 TempPos = new Vector3(-0.5f + LD.PlayerPosition[i - 1, 0] * 0.5f, -0.5f + LD.PlayerPosition[i - 1, 1] * 0.5f, 0);
                TempObj = Instantiate(Player, TempPos, ZeroQuaternion);
                TempObj.name = "Player_" + i;
                TempObj.GetComponentsInChildren<SpriteRenderer>()[0].color = LD.PlayerColors[i - 1];
                TempObj.GetComponentsInChildren<SpriteRenderer>()[0].enabled = false;
                TempObj.GetComponentsInChildren<SpriteRenderer>()[1].enabled = false;
            }
        }

        //Выгрузить загруженные ресурсы
        Resources.UnloadUnusedAssets();
    }
    
    //НЕ ЛЕЗЬ, ОНА ТЕБЯ СОЖРЁТ
    public void Generate(int j)
    {
        int AmountOfEl = 1;
        if (j != 2 * m - 1)
        {
            for (int i = 2 + OffsetX; i < 2 * n + OffsetX; i += 2)
                if (LD.Arr[i - 1, j] == LD.Arr[i + 1, j])
                    LD.Arr[i, j] = 1;
            for (int i = 2 + OffsetX; i <= 2 * n + OffsetX; i += 2)
            {
                int CurrentElement = LD.Arr[i - 1, j];
                if (i == 2 * n + OffsetX)
                {
                    Close(AmountOfEl, i, j);
                }
                else
                {
                    bool MakeWall = (LD.Seeder.Next(2) > 0 ? true : false);
                    if (MakeWall)
                    {
                        LD.Arr[i, j] = 1;
                        Close(AmountOfEl, i, j);
                        AmountOfEl = 1;
                    }
                    else
                    {
                        LD.Arr[i + 1, j] = CurrentElement;
                        AmountOfEl++;
                    }
                }
            }
        }
        else
        {
            for (int i = 1 + OffsetX; i < 2 * n - 1 + OffsetX; i += 2)
                for (int i1 = i + 2 + OffsetX; i1 < 2 * n + OffsetX; i1 += 2)
                    if (LD.Arr[i, j] == LD.Arr[i1, j])
                        LD.Arr[i + 1, j] = 1;
        }
    }

    //Кусочек магии
    //Закрытие получившегося кусочка "шапочками"
    public void Close(int AmountOfEl, int i, int j)
    {
        bool FreeSpace;
        do
        {
            FreeSpace = false;
            for (int i1 = (i + 1 - AmountOfEl * 2); i1 < i; i1 += 2)
            {
                LD.Arr[i1, j + 1] = (LD.Seeder.Next(AmountOfEl) > 0 ? 1 : 0);
                if (LD.Arr[i1, j + 1] == 0)
                    FreeSpace = true;
            }
        }
        while (!FreeSpace);
        for (int i1 = (i + 1 - AmountOfEl * 2); i1 < i; i1 += 2)
            if (LD.Arr[i1, j + 1] == 0)
            {
                if (j == 1)
                    LD.Arr[i1, j + 2] = LD.Arr[i1, j];
                else
                {
                    for (int i2 = i - 1; i2 >= (i + 1 - AmountOfEl * 2); i2 -= 2)
                    {
                        if ((LD.Arr[i2, j - 1] == 0) && (LD.Arr[i1, j + 2] > LD.Arr[i2, j - 2]))
                        {
                            LD.Arr[i1, j + 2] = LD.Arr[i2, j - 2];
                        }
                    }
                }
            }
    }

    //Размещение выходов из лабиринта
    public void PlaceExits()
    {
        bool Flag;
        do
        {
            Flag = true;
            int x1 = LD.Seeder.Next(1, n + 1), x2 = LD.Seeder.Next(1, n + 1),
            y1 = LD.Seeder.Next(1, m + 1), y2 = LD.Seeder.Next(1, m + 1);
            LD.Arr[x1 * 2 - 1 + OffsetX, 2 * m] = 0;
            LD.Arr[x2 * 2 - 1 + OffsetX, 0] = 0;
            LD.Arr[2 * n + OffsetX, y1 * 2 - 1] = 0;
            LD.Arr[OffsetX, y2 * 2 - 1] = 0;
            if (((LD.Arr[1 + OffsetX, 2 * m] == 0) && (LD.Arr[OffsetX, 2 * m - 1] == 0)) ||
                ((LD.Arr[1 + OffsetX, 0] == 0) && (LD.Arr[OffsetX, 1] == 0)) ||
                ((LD.Arr[2 * n - 1 + OffsetX, 0] == 0) && (LD.Arr[2 * n + OffsetX, 1] == 0)) ||
                ((LD.Arr[2 * n - 1 + OffsetX, 2 * m] == 0) && (LD.Arr[2 * n + OffsetX, 2 * m - 1] == 0)))
            {
                Flag = false;
                LD.Arr[x1 * 2 - 1 + OffsetX, 2 * m] = 1;
                LD.Arr[x2 * 2 - 1 + OffsetX, 0] = 1;
                LD.Arr[2 * n + OffsetX, y1 * 2 - 1] = 1;
                LD.Arr[OffsetX, y2 * 2 - 1] = 1;
            }
        } while (!Flag);
    }

    //Размещение ям
    public void PlacePits(ref bool OkFlag)
    {
        int Counter = 0;
        int[,] PitPos = new int[2, m * n];
        for (int j = 1; j < 2 * m; j += 2)
            for (int i = 1 + OffsetX; i < 2 * n + OffsetX; i += 2)
            {
                int WallsAround = 0;
                if (LD.Arr[i + 1, j] == 1)
                    WallsAround++;
                if (LD.Arr[i - 1, j] == 1)
                    WallsAround++;
                if (LD.Arr[i, j + 1] == 1)
                    WallsAround++;
                if (LD.Arr[i, j - 1] == 1)
                    WallsAround++;
                if (WallsAround >= 3)
                {
                    PitPos[0, Counter] = i;
                    PitPos[1, Counter++] = j;
                    LD.Arr[i, j] = 2;
                }
            }

        if (Counter >= pt)
        {
            OkFlag = true;
            for (int i = 1; i <= pt; i++)
            {
                bool Placed;
                do
                {
                    Placed = false;
                    int Choice = LD.Seeder.Next(0, Counter);
                    if (LD.Arr[PitPos[0, Choice], PitPos[1, Choice]] == 2)
                    {
                        LD.Arr[PitPos[0, Choice], PitPos[1, Choice]] += i;
                        Placed = true;
                    }
                } while (!Placed);

                do
                {
                    Placed = false;
                    int ChoiceI = LD.Seeder.Next(1, n + 1) * 2 - 1 + OffsetX, ChoiceJ = LD.Seeder.Next(1, m + 1) * 2 - 1;
                    if (LD.Arr[ChoiceI, ChoiceJ] == 0)
                    {
                        LD.Arr[ChoiceI, ChoiceJ] += pt + i;
                        Placed = true;
                    }
                } while (!Placed);
            }
        }

        if (Counter > 0)
        {
            for (int i = 0; i < Counter; i++)
            {
                LD.Arr[PitPos[0, i], PitPos[1, i]] -= 2;
            }
        }
    }

    //Размещение <item> на свободное место в количестве <AmountOfItemsToBePlaced>
    public void PlaceOnClear(int AmountOfItemsToBePlaced, int item)
    {
        bool Placed;
        do
        {
            do
            {
                Placed = false;
                int PositionX = LD.Seeder.Next(1, n + 1) * 2 - 1 + OffsetX,
                PositionY = LD.Seeder.Next(1, m + 1) * 2 - 1;
                if (LD.Arr[PositionX, PositionY] == 0)
                {
                    LD.Arr[PositionX, PositionY] = item;
                    Placed = true;
                    AmountOfItemsToBePlaced--;
                }
            } while (!Placed);
        } while (AmountOfItemsToBePlaced != 0);
    }

}