// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class GPGSIds
{
        public const string achievement_thats_a_harder_one = "CgkIqt-ruo4XEAIQAw"; // <GPGSID>
        public const string leaderboard_top_players_extreme = "CgkIqt-ruo4XEAIQCQ"; // <GPGSID>
        public const string leaderboard_top_players_hard = "CgkIqt-ruo4XEAIQCA"; // <GPGSID>
        public const string achievement_easy_round = "CgkIqt-ruo4XEAIQAg"; // <GPGSID>
        public const string achievement_hard_as_a_rock = "CgkIqt-ruo4XEAIQBA"; // <GPGSID>
        public const string achievement_psycho = "CgkIqt-ruo4XEAIQBQ"; // <GPGSID>
        public const string leaderboard_top_players_normal = "CgkIqt-ruo4XEAIQBw"; // <GPGSID>
        public const string achievement_ready_for_playing = "CgkIqt-ruo4XEAIQAQ"; // <GPGSID>
        public const string leaderboard_top_players_easy = "CgkIqt-ruo4XEAIQBg"; // <GPGSID>

}

